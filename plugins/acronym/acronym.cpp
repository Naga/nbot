#include "acronym.h"

#if defined(HAS_STD_REGEX)
	#include <regex>
	#define REGEX_NAMESPACE std
#elif defined(HAS_BOOST)
	#include "boost/xpressive/xpressive.hpp"
	#define REGEX_NAMESPACE boost::xpressive
#else
	#error "Need one of std::regex or boost::xpressive!"
#endif

void acronym_plugin::OnPrivMessage(IRCChannel *chan, IRCUser *user, IRCMessage * msg) {
	NString cmd;
	NString acr;
	std::vector<NString> definitions;
	
	if (msg->message.Token(0).Trim_r() != "!acr") {
           return;
    }
	acr = msg->message.Token(1).Trim_r();
	acr = acr.asUpper();
	
	if(acrcache.find(acr) != acrcache.end()) {
		definitions = acrcache[acr];
	} else {
		SearchAcronyms(acr);
		definitions = acrcache[acr];
	}
		
	if (definitions.empty()) {
		session->PutServ("PRIVMSG %s :Acronym '%s' not found!", chan->getName().c_str(), acr.c_str());
		return;
	}
	
	session->PutServ("PRIVMSG %s : Acronym %s means: (%d found, max. 3 shown)", chan->getName().c_str(), acr.c_str(), definitions.size());
	
	int max = (definitions.size() >= 3) ? 3 : definitions.size();
	for (unsigned int i = 0; i < max; ++i) {
		NString desc = definitions[i];
		session->PutServ("PRIVMSG %s : %d: %s", chan->getName().c_str(), i+1, desc.c_str());
	}
}

void acronym_plugin::SearchAcronyms(NString acr) {
	NSocket *socket = new NSocket("www.acronymfinder.com", 80);
	if (!socket->start()) {
		std::cout << "[acronym_plugin]: cannot connect to www.acronymfinder.com" << std::endl;
		return;
	}

	std::stringstream request;
	request << "GET /"<< acr.c_str() << ".html HTTP/1.1\r\n";
	request << "Host: www.acronymfinder.com:80 \r\n";
	request << "Connection: close\r\n\r\n";

	socket->Send(request.str());
	socket->waitfordata(30);
	
	NString response;
	socket->Recv(response, socket->available());
	
#if defined(HAS_STD_REGEX)
	REGEX_NAMESPACE::regex pattern = REGEX_NAMESPACE::regex(">"+acr.asUpper()+"</a></td><td>(.*?)</td><td.*?<script>");
	REGEX_NAMESPACE::regex special_result_pattern = REGEX_NAMESPACE::regex("Location: /(.*?).html");
	REGEX_NAMESPACE::regex num_pattern = REGEX_NAMESPACE::regex("Your abbreviation search returned ([0-9]+) meanings");
#elif defined(HAS_BOOST)
	REGEX_NAMESPACE::sregex pattern = REGEX_NAMESPACE::sregex::compile(">"+acr.asUpper()+"</a></td><td>(.*?)</td><td.*?<script>");
	REGEX_NAMESPACE::sregex special_result_pattern = REGEX_NAMESPACE::sregex::compile("Location: /(.*?).html");
	REGEX_NAMESPACE::sregex num_pattern = REGEX_NAMESPACE::sregex::compile("Your abbreviation search returned ([0-9]+) meanings");
#endif

	REGEX_NAMESPACE::smatch matches;
	if (!REGEX_NAMESPACE::regex_search(response, matches, num_pattern)) {
		std::cout << response.c_str() << std::endl;
		if (REGEX_NAMESPACE::regex_search(response, matches, special_result_pattern))
			acrcache[acr.asUpper()].push_back(matches[1].str());
		return;
	}

	const REGEX_NAMESPACE::sregex_iterator End;
	for (REGEX_NAMESPACE::sregex_iterator i(response.begin(), response.end(), pattern); i != End; ++i)	{
		if ((*i).size() < 2)
			continue; //failsafe
		NString desc = (*i)[1].str();
		int spos = desc.find("<a");
		int sepos = desc.find_first_of(">", spos);
		if (spos != std::string::npos && sepos != std::string::npos) {
			desc.erase(spos, sepos+1);
			int epos = desc.find("</a>");
			if (epos != std::string::npos)
				desc.erase(epos, epos+4);
		}
		acrcache[acr.asUpper()].push_back(desc); 
	}

	return;
}

void acronym_plugin::OnLoad(IRCSession *session) { //implement this to be able to load the plugin at "runtime"
	this->session = session;
}

MODULE_EXPORT CPlugin *getModule() {
	CPlugin *module = new acronym_plugin();
	module->setInfo("acronymfinder", "searches and posts the definition of an acronym");
	return module;
}