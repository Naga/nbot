#include <iostream>
#include <map>
#include <vector>

#include "CPlugin.h"
#include "CIRCMessage.h"
#include "CIRCSession.h"
#include "NString.h"

class acronym_plugin : public CPlugin {
private:
	std::map<NString, std::vector<NString>> acrcache;
	IRCSession *session;
	void SearchAcronyms(NString acr);
public:
	void OnLoad(IRCSession *session);
	void OnPrivMessage(IRCChannel *chan, IRCUser *user, IRCMessage * msg);
};