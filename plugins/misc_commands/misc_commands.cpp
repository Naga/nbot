#include "defines.h"
#include "misc_commands.h"
NString misc_commands_plugin::getRndSlapStr() {
	NString things_to_slap_with[] = {
									"HP-ProLiant DL360 G5",
									"DDR1 RAM",
									"Standleitung",
									"Glasfaserkabel",
									"PoE-Switch",
									"plugin-memberfunction",
									"segfault",
									"fifty pound UNIX manual",
									"Windows 98 bug list",
									"trout-shaped anvil!",
									"Khaled Mardam-Bey",
									"mIRC 6.01",
									"naked picture of Bill Gates",
									"MS IIS Server",
									"QuakeNET server",
									"set of Windows 3.11 floppies",
									"Compaq laptop",
									"10mbit network card"
									};
	srand(time(0));
	int rand_idx = rand() % (sizeof(things_to_slap_with)/sizeof(things_to_slap_with[0]));
	return things_to_slap_with[rand_idx];
}

NString misc_commands_plugin::getRndSlapAction() {
	NString slap_actions[] = {
									"slaps",
									"violates",
									"hurts",
									"tortures",
									"hits",
									"kills"
									};
	srand(time(0));
	int rand_idx = rand() % (sizeof(slap_actions)/sizeof(slap_actions[0]));
	return slap_actions[rand_idx];
}

void misc_commands_plugin::OnPrivMessage(IRCChannel *chan, IRCUser *user, IRCMessage * msg) {
	NString cmd;
	if (msg->message.find("^.^") == std::string::npos)
		return;

	session->PutServ("PRIVMSG %s :\001ACTION %s %s with a %s\001", chan->getName().c_str(), getRndSlapAction().c_str(), user->getNick().c_str(), getRndSlapStr().c_str());
}

void misc_commands_plugin::OnCTCPAction(IRCChannel *chan, IRCUser *user, IRCMessage *msg) {
	NString cmd;
	if(msg->message.find(session->getBotNick()) == std::string::npos)
		return; //dont trigger if the bot issnt the target
	
	if ((msg->message.find(" slap ") == std::string::npos) && (msg->message.find(" slaps ") == std::string::npos))
		return;

	session->PutServ("PRIVMSG %s :\001ACTION %s %s with a %s\001", chan->getName().c_str(), getRndSlapAction().c_str(), user->getNick().c_str(), getRndSlapStr().c_str());
}

void misc_commands_plugin::OnConnected(IRCSession *sess) {
	this->session = sess;
}
void misc_commands_plugin::OnLoad(IRCSession *sess) {
	this->session = sess;
}

MODULE_EXPORT CPlugin *getModule() {
	CPlugin *module = new misc_commands_plugin();
	module->setInfo("misc_commands", "Just some commands that diddnt fit into other plugins");
	return module;
}