
#include "defines.h"
#include "CPlugin.h"
#include "CIRCMessage.h"
#include "CIRCSession.h"
#include "CIRCChannel.h"

class misc_commands_plugin : public CPlugin {
	IRCSession *session;
	void OnPrivMessage(IRCChannel *chan, IRCUser *user, IRCMessage * msg);
	void OnCTCPAction(IRCChannel *chan, IRCUser *user, IRCMessage * msg);
	void OnConnected(IRCSession *session);
	void OnLoad(IRCSession *session);
	NString getRndSlapStr();
	NString getRndSlapAction();
};