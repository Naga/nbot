#include "CPlugin.h"
#include "CIRCMessage.h"
#include "CIRCSession.h"
#include "NString.h"

struct choice {
	time_t timestamp;
	std::string question;
	std::string answer;
};

class choice_plugin : public CPlugin {
	IRCSession *session;
	std::vector<choice> saved_questions;

	void OnPrivMessage(IRCChannel *chan, IRCUser *user, IRCMessage * msg);
	void OnConnected(IRCSession *session);
	void OnLoad(IRCSession *session);
};
	