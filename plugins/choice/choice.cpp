#include "choice.h"
#include "defines.h"

#if defined(HAS_STD_REGEX)
	#include <regex>
#elif defined(HAS_BOOST)
	#include "boost/xpressive/xpressive.hpp"
	//#include <boost/regex.hpp>
#else
	#error "Need one of std::regex or boost::xpressive!"
#endif

void choice_plugin::OnPrivMessage(IRCChannel *chan, IRCUser *user, IRCMessage * msg) {

	//CConfig * plugincfg = session->getConfig()->getPluginConfig("choice");

	if (session->getConfig()->GetBoolean("ignore_bots") && user->getModes().find("B") != std::string::npos)
		return;

	int count = msg->message.find_count("[]")+msg->message.find_count("[ ]");
	if (count < 2)
		return;

	time_t ts = time(NULL);
	std::vector<choice>::iterator it = saved_questions.begin();
	NString found_answer("");

	while(it != saved_questions.end()) {
		if ((ts - (it)->timestamp) > 1800) {
			it = saved_questions.erase(it);
		} else {
			if ((it)->question == msg->message)
				found_answer = (it)->answer;
			++it;
		}
	}


	if (!found_answer.empty()) {
		session->PutServ("PRIVMSG %s :%s", chan->getName().c_str(), found_answer.c_str());
		return;
	}

#if defined(HAS_STD_REGEX)
	std::regex pattern = std::regex("([a-zA-Z0-9_ ]+)");
	std::sregex_token_iterator cur( msg->message.begin(), msg->message.end(), pattern, 1 );
	std::sregex_token_iterator end;
#elif defined(HAS_BOOST)
	boost::xpressive::sregex pattern = boost::xpressive::sregex::compile("([a-zA-Z0-9_ ]+)");
	boost::xpressive::sregex_token_iterator cur( msg->message.begin(), msg->message.end(), pattern, 1 );
	boost::xpressive::sregex_token_iterator end;
#endif	


	std::vector<NString> options;
	for( ; cur != end; ++cur ) {
		NString match = (*cur).str();
		match.Trim();
			if (!match.empty() && find(options.begin(), options.end(), match) == options.end())
				options.push_back(match);
	}

	if (options.size() == 0)
		return;

	if (options.size() != count)
		std::cout << "[choice_plugin]: Size differs from count! " << options.size() << " " << count << std::endl;

	srand((unsigned)time(NULL));
	int rnd =(rand()%options.size());
	NString send;
	for (unsigned int i = 0; i < options.size(); ++i)
		if (i == rnd)
			send += options[i]+" [x] ";
		else
			send += options[i]+" [] ";

	choice newchoice;
	newchoice.timestamp = time(NULL);
	newchoice.question = msg->message;
	newchoice.answer = send;
	saved_questions.push_back(newchoice);

	session->PutServ("PRIVMSG %s :%s", chan->getName().c_str(), send.c_str());
}

void choice_plugin::OnConnected(IRCSession *session) {
	this->session = session;
}

void choice_plugin::OnLoad(IRCSession *session) { //implement this to be able to load the plugin at "runtime"
	this->session = session;
}

MODULE_EXPORT CPlugin *getModule() {
	CPlugin *module = new choice_plugin();
	module->setInfo("choice", "plugin that answers on \"should I do this [] or this []\" questions");
	return module;
}