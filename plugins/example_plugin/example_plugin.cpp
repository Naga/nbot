#include "example_plugin.h"
#include "CPlugin.h"
#include "CIRCMessage.h"
#include "CIRCSession.h"
#include "EventMgr.h"

void example(CPlugin* plugin, Event* e);

class example_plugin : public CPlugin {
	IRCSession *session;
	void OnPing(IRCMessage *msg) {
		std::cout << "[module]: Pinged!" << std::endl;
	}
	void OnConnected(IRCSession *session) {
		this->session = session;
		EventMgr::getInstance()->RegisterPluginCallback(EVENT_ONPING, this, example);
	}
	void OnLoad(IRCSession *session) { //implement this to be able to load the plugin at "runtime"
		this->session = session;
	}
public:
	void callme(Event* e) {
		std::cout << "I got called from the function!" << std::endl;
	}
};


void example(CPlugin* plugin, Event* e) {
	//do something
	example_plugin * mod = (example_plugin*)plugin;
	mod->callme(e);
}






MODULE_EXPORT CPlugin *getModule() {
	CPlugin *module = new example_plugin();
	module->setInfo("testmodule", "just a simple testmodule");
	return module;
}