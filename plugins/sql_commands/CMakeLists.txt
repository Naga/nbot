SET(MODULE_SRC
				sql_commands.cpp
				sql_commands.h
)
SET(LIBRARY_OUTPUT_PATH ${CMAKE_SOURCE_DIR})
ADD_LIBRARY("sql_commands" SHARED ${MODULE_SRC})