#include "defines.h"
#include "sql_commands.h"

void sql_command_plugin::OnPrivMessage(IRCChannel *chan, IRCUser *user, IRCMessage * msg) {
	NString cmd;
	if (msg->message[0] == '!') {  //custom triggers? TODO: implement
            cmd = msg->message.substr(0, msg->message.find(" "));
    }

	if (cmd == "!reload") {
		if (user->getNick() != session->getConfig()->GetString("bot_owner").c_str()) {
			session->PutServ("NOTICE %s :not allowed", user->getNick().c_str());
			return;
		}
		Reload();
		return;
	}

	if (sql_commands.find(cmd) == sql_commands.end())
		return;

	NString arg = (cmd.length()+1 < msg->message.length()) ? msg->message.substr(cmd.length()+1, std::string::npos) : "";
	NString str("");
	std::vector<NString> args = arg.split_delim(" ");
	SQLCommand *sqlcmd = sql_commands[cmd];
	NString text = sqlcmd->text;

	if (text.find("<reqrealtarget>") != std::string::npos)
		if (args.size() > 0) {
			if (!chan->isUserOnChan(args[0])) {
					session->PutServ("NOTICE %s : Wo ist %s?", user->getNick().c_str(), args[0].c_str());
			}
			text.erase(text.find("<reqrealtarget>"), text.find("<reqrealtarget>")+15);
		} else {
			session->PutServ("NOTICE %s : Ziel fehlt! Syntax: \"%s nickname\"", user->getNick().c_str(), cmd.c_str());
			return;
		}

	for(unsigned int i = 1; i <= 10; ++i) {
		std::stringstream tmp;
		tmp << "<arg" << i << ">";
		while (text.find(tmp.str()) != std::string::npos)
			text.replace(text.find(tmp.str()), tmp.str().size(), (args.size() >= i ? args[i-1] : ""));
	}
	if (text.find("<nick>") != std::string::npos)
		text.replace(text.find("<nick>"), 6, user->getNick().c_str());


	std::vector<NString> parts = text.split_delim("\n");

	for (std::vector<NString>::const_iterator it = parts.begin(); it != parts.end(); ++it) {
		NString textpart = *it;
		switch(sqlcmd->type) {
			case 1:
				str="PRIVMSG "+chan->getName()+" :\001ACTION "+textpart+"\001";
				break;
			case 2:
				str="NOTICE "+user->getNick()+" :"+textpart;
				break;
			case 0:
				str = "PRIVMSG "+chan->getName()+" :"+textpart;
				break;
		}


		session->PutServ(str.c_str());
	}
}

void sql_command_plugin::Reload() {
	sql_commands.clear();
}
void sql_command_plugin::LoadSQLCmds() {
	CServerConfig *config = session->getConfig();
	CPluginConfig *plugincfg = config->getPluginConfig("sql_commands");

	sql_commands.clear();
	MYSQL *connection, mysql;
	MYSQL_RES *res; //pointer to the result set structure
	MYSQL_ROW row; //row information
        
	mysql_init(&mysql);
	mysql_options(&mysql,MYSQL_OPT_RECONNECT,"1");
	connection = mysql_real_connect(&mysql,plugincfg->GetCString("mysql_host"),plugincfg->GetCString("mysql_user"),plugincfg->GetCString("mysql_pass"),plugincfg->GetCString("mysql_db"),0,NULL,0);
	if (connection == NULL) {
			std::cout << "[SQLCommandPlugin]: Failed to Connect error: " << mysql_error(&mysql) << std::endl;
			return;
	}

	mysql_query(connection, "SELECT * FROM cmds;");
	res = mysql_store_result(connection);

	while (row = mysql_fetch_row(res)) {
			SQLCommand* cmd= new SQLCommand();
			cmd->type = (unsigned short) strtoul(row[2], NULL, 0);
			cmd->text = row[3];
			cmd->cmd = row[1];
			sql_commands.insert(std::pair<NString, SQLCommand*>(cmd->cmd, cmd));
	}

	std::cout << "[SQLCommandPlugin]: Loaded " << sql_commands.size() << " Commands from SQL" << std::endl;
	mysql_close(connection);
}

void sql_command_plugin::OnLoad(IRCSession *session) { //implement this to be able to load the plugin at "runtime"
	this->session = session;
	LoadSQLCmds();
}

MODULE_EXPORT CPlugin *getModule() {
	CPlugin *module = new sql_command_plugin();
	module->setInfo("SQLCommands", "Loads simple ! commands from sql");
	return module;
}