
#include "defines.h"
#include "CPlugin.h"
#include "CIRCMessage.h"
#include "CIRCSession.h"
#include "CIRCChannel.h"

#ifdef HAS_MYSQL
#	include "mysql.h"
#endif

#if defined(HAS_STD_REGEX)
	#include <regex>
#elif defined(HAS_BOOST)
	#include "boost/xpressive/xpressive.hpp"
#else
	#error "Need one of std::regex or boost::xpressive!"
#endif

#ifdef HAS_MYSQL

struct SQLCommand{
	NString cmd;
	unsigned short int type;
	NString text;
};

class sql_command_plugin : public CPlugin {
	IRCSession *session;
	std::map<NString, SQLCommand*> sql_commands;

	void OnPrivMessage(IRCChannel *chan, IRCUser *user, IRCMessage * msg);
	void OnLoad(IRCSession *session);
	void Reload();
	void LoadSQLCmds();
};

#endif