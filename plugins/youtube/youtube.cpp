#include "defines.h"
#include "youtube.h"
#include "CPlugin.h"
#include "NSocket.h"
#include "CIRCMessage.h"
#include "CIRCSession.h"
#include "CIRCChannel.h"


#if defined(HAS_STD_REGEX)
	#include <regex>
	#define REGEX_NAMESPACE std
#elif defined(HAS_BOOST)
	#include "boost/xpressive/xpressive.hpp"
	#define REGEX_NAMESPACE boost::xpressive
#else
	#error "Need one of std::regex or boost::xpressive!"
#endif

class youtube_plugin : public CPlugin {
	IRCSession *session;

	void OnPrivMessage(IRCChannel *chan, IRCUser *user, IRCMessage * msg) {
	
		if (msg->message.find("youtube.") == std::string::npos && msg->message.find("youtu.be") == std::string::npos)
			return; //performance

		NString m = msg->message;
		NString strpattern;

		if (m.find("youtu.be") != std::string::npos) {
			strpattern = "(www\\.)?(youtu\\.(be)/([a-zA-Z0-9_-]+))";
		} else {
			strpattern = "(www\\.)?(youtube\\.(de|com))/watch.*[\\?|&]v=([a-zA-Z0-9_-]+)";
		}

		#if defined(HAS_STD_REGEX)
			REGEX_NAMESPACE::regex pattern = std::regex(strpattern);
		#elif defined(HAS_BOOST)
			REGEX_NAMESPACE::sregex pattern = boost::xpressive::sregex::compile(strpattern);
		#endif
		
		REGEX_NAMESPACE::smatch matches;
		
		if (!REGEX_NAMESPACE::regex_search(m, matches, pattern))
			return;

		NString id = matches[4].str();
		if (id.empty())
			return;

		NSocket *socket = new NSocket("gdata.youtube.com", 80);
		socket->start();

		CPluginConfig * plugincfg = session->getConfig()->getPluginConfig("youtube");

		std::stringstream request;
		request << "GET /feeds/api/videos/" << id.c_str() << "?v=2" << " HTTP/1.1\r\n";
		request << "Host: gdata.youtube.com:80\r\n";
		request << "Connection: close\r\n\r\n";

		socket->Send(request.str());

		NString tmp;
		socket->RecvUntil(tmp, "</entry>");

		int titlestart = tmp.find("<title>")+7;
		int titleend = tmp.find("</title>", titlestart);

		int ratingstart	= tmp.find("<gd:rating average='")+20;
		int ratingend	= tmp.find_first_of("'", ratingstart);

		if (titleend == std::string::npos || ratingend == std::string::npos)
			return;

		NString title = tmp.substr(titlestart, titleend-titlestart);
		std::istringstream iss(tmp.substr(ratingstart, ratingend-ratingstart));
		double rating;
		iss >> rating;
		title.htmlEntitiesDecode();

		NString format("PRIVMSG %s :youtube> ");

		if (plugincfg->GetBoolean("print_title"))
			format += "%s";
		if (plugincfg->GetBoolean("print_rating"))
			format += " | Rating: %0.1f/5.0";

		session->PutServ(format.c_str(), chan->getName().c_str(), title.c_str(), rating);
	}

	void OnConnected(IRCSession *session) {
		this->session = session;
	}
	void OnLoad(IRCSession *session) {
		this->session = session;
	}
};


MODULE_EXPORT CPlugin *getModule() {
	CPlugin *module = new youtube_plugin();
	module->setInfo("youtube", "Plugin to show youtube info (title, rating) if someone posts a youtube link");
	return module;
}
