#include "lua_scripts.h"
#include "CPlugin.h"
#include "CIRCMessage.h"
#include "CIRCSession.h"
#include "EventMgr.h"
#include <dirent.h>

extern "C" {
# include "lua.h"
# include "lauxlib.h"
# include "lualib.h"
}
#include "external/LuaBridge/LuaBridge.h"

enum load_errors {
	ERR_NONE = 0,
	ERR_FATAL,
	ERR_SOME
};

using namespace luabridge;
IRCSession *cur_session = NULL;

std::map<NString,lua_State *>Lmap;

inline bool ends_with(std::string const & value, std::string const & ending) {
    if (ending.size() > value.size()) return false;
    return std::equal(ending.rbegin(), ending.rend(), value.rbegin());
}

bool unloadLuaScript(const char *name) {
	for (std::map<NString,lua_State*>::iterator it = Lmap.begin(); it != Lmap.end(); ++it) {
		if ((*it).first == NString(name).asUpper()) {
			lua_State* lstate = (*it).second;
			std::cout << "[LUA]: Unloading script: " << (*it).first << std::endl;
			lua_close(lstate);
			Lmap.erase(it);
			return true;
		}
	}
	return false;
}

void callLuaEvent(const char *name, NString chan="", NString user="", NString message="") {
	for(auto pair : Lmap) {
		NString script_name = pair.first;
		lua_State* lstate = pair.second;
		//std::cout << "[LUA]: Calling Event: " << name << " for script " << script_name << std::endl;
		LuaRef ref = getGlobal(lstate, name );
		if(ref) {
			try {
				if (name == "OnPrivMessage")
					(ref)(chan.c_str(), user.c_str(), message.c_str());
				else
					(ref)();
			}
			catch ( LuaException e ) {
				std::cerr << "( ERROR )" << e.what() << std::endl;
			}
		}
	}
}



void PutChan(const char *chan, const char *str) {
	if (cur_session)
		cur_session->PutServ("PRIVMSG %s :%s", chan, str);
}
void PutServ(const char *str) {
	if (cur_session)
		cur_session->PutServ(str);
}
void PutQuick(const char *str) {
	if (cur_session)
		cur_session->PutQuick(str);
}

load_errors LoadScripts(NString path, bool reload=false) {
	bool errors = false;
	if (reload) {
		for (std::map<NString,lua_State*>::iterator it = Lmap.begin(); it != Lmap.end(); ++it) {
			lua_State* lstate = (*it).second;
			lua_close(lstate);
			Lmap.erase(it);
		}
	}


	auto dir = opendir(path.c_str());
	if (!dir) {
		std::cerr << "LoadLuaScripts: cannot access " << path << std::endl;
		return ERR_FATAL;
	}
	
	struct dirent * entity;
	while((entity = readdir( dir )) != NULL ) {
		if ( !strcmp( entity->d_name, "."  ) || !strcmp( entity->d_name, ".." ) || entity->d_name[0] == '.')
			continue;
		
		if(entity->d_type == DT_REG) {
			if (ends_with(entity->d_name, ".lua")) {
				lua_State *L = luaL_newstate();
				luaL_openlibs(L);
				NString file = path+entity->d_name;
				int status = luaL_loadfile(L, file.c_str());

				if ( status!=0 ) {
				  std::cerr << "Failed to load " << file << "(" << status << ") Error: " << lua_tostring(L, -1) << std::endl;
				  lua_close(L);
				  errors = true;
				  continue;
				}
				
				
				getGlobalNamespace(L).addFunction("PutServ", &PutServ);
				getGlobalNamespace(L).addFunction("PutChan", &PutChan);
				//init execute
				lua_pcall(L, 0, 0, 0);
				Lmap.insert(std::pair<NString,lua_State*>(NString(entity->d_name).asUpper(),L));
				std::cout << "- Loaded Script " << entity->d_name << std::endl;
			}
		}
	}
	return (errors) ? ERR_SOME : ERR_NONE;
}

class lua_scripts : public CPlugin {
	void OnPing(IRCMessage *msg) {
		callLuaEvent("OnPing");
	}
	void OnConnected(IRCSession *session) {
		cur_session = session;
	}
	void OnLoad(IRCSession *session) { //implement this to be able to load the plugin at "runtime"
		cur_session = session;
		
		CPluginConfig *plugincfg = cur_session->getConfig()->getPluginConfig("lua_scripts");
		NString path = plugincfg->GetCString("script_path");
		LoadScripts(path);
		
		callLuaEvent("OnLoad");
	}
	
	void OnPrivMessage(IRCChannel *chan, IRCUser *user, IRCMessage * msg) {
	
		if (msg->message.Token(0).Trim_r() == "!lua") {
           	NString cmd = msg->message.Token(1).Trim_r().asUpper();
			NString target = msg->message.Token(2).Trim_r();
			
			
			if (cmd == "UNLOAD") {
				if (unloadLuaScript(target.c_str()))
					cur_session->PutServ("PRIVMSG %s :Script '%s' unloaded.", chan->getName().c_str(), target.c_str());
				else
					cur_session->PutServ("PRIVMSG %s :Script '%s' failed to unload!", chan->getName().c_str(), target.c_str());
					
				return;
			}
			if (cmd == "RELOAD") {
				CPluginConfig *plugincfg = cur_session->getConfig()->getPluginConfig("lua_scripts");
				NString path = plugincfg->GetCString("script_path");
				cur_session->PutServ("PRIVMSG %s :Reloading lua scripts...", chan->getName().c_str());
				
				load_errors err = LoadScripts(path, /*reload=*/true);
				
				if(err == ERR_NONE)
					cur_session->PutServ("PRIVMSG %s :Done loading %d scripts.", chan->getName().c_str(), Lmap.size());
				else if (err == ERR_FATAL)
					cur_session->PutServ("PRIVMSG %s :Fatal Error reloading scripts!", chan->getName().c_str());
				else if (err == ERR_SOME)
					cur_session->PutServ("PRIVMSG %s :Error reloading some scripts!", chan->getName().c_str());
				
					
					
				return;
			}
			if (cmd == "MEMORY") {
				int usage = 0;
				for(auto pair : Lmap) {
					if (pair.second) {
						lua_State* lstate = pair.second;
						usage += lua_gc(lstate, LUA_GCCOUNT, 0);
					}
				}
				cur_session->PutServ("PRIVMSG %s :Lua Memory usage: %d KB.", chan->getName().c_str(), usage);
				return;
			}

			std::cout << "[LUA]: !lua " << cmd << " " << target << std::endl;
		}
	
	
	
		callLuaEvent("OnPrivMessage", chan->getName(), user->getNick(), msg->message);
	}
};

MODULE_EXPORT CPlugin *getModule() {
	CPlugin *module = new lua_scripts();
	module->setInfo("lua_scripts", "lua scripting module");
	return module;
}