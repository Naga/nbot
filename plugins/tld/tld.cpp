#include "tld.h"

#if defined(HAS_STD_REGEX)
	#include <regex>
	#define REGEX_NAMESPACE std
#elif defined(HAS_BOOST)
	#include "boost/xpressive/xpressive.hpp"
	#define REGEX_NAMESPACE boost::xpressive
#else
	#error "Need one of std::regex or boost::xpressive!"
#endif

void tld_plugin::OnPrivMessage(IRCChannel *chan, IRCUser *user, IRCMessage * msg) {
	NString cmd;
	NString tld;
	if (msg->message.Token(0).Trim_r() != "!tld") {
           return;
    }
	tld = msg->message.Token(1).Trim_r();
	if (tld.find(".") == std::string::npos)
		tld="."+tld.asLower();
		
	NString desc = tldmap[tld];
		
	if (desc.empty()) {
		session->PutServ("PRIVMSG %s :TLD '%s' not found", chan->getName().c_str(), tld.c_str());
		return;
	}
		
	session->PutServ("PRIVMSG %s :%s - %s", chan->getName().c_str(), tld.c_str(), desc.c_str());
}
void tld_plugin::ParseTlds() {
	NSocket *socket = new NSocket("www.countries-ofthe-world.com", 80);
	if (!socket->start()) {
		std::cout << "[TLD]: cannot connect to www.countries-ofthe-world.com" << std::endl;
		return;
	}

	std::stringstream request;
	request << "GET /TLD-list.html HTTP/1.1\r\n";
	request << "Host: www.countries-ofthe-world.com:80 \r\n";
	request << "Connection: close\r\n\r\n";

	socket->Send(request.str());
	socket->waitfordata(30);
	
	NString response;
	socket->Recv(response, socket->available());
	
#if defined(HAS_STD_REGEX)
	REGEX_NAMESPACE::regex pattern = REGEX_NAMESPACE::regex("<tr><td class=\"letter\">(.*?)</td><td>(.*?)</td></tr>");
#elif defined(HAS_BOOST)
	REGEX_NAMESPACE::sregex pattern = REGEX_NAMESPACE::sregex::compile("<tr><td class=\"letter\">(.*?)</td><td>(.*?)</td></tr>");
#endif

	const REGEX_NAMESPACE::sregex_iterator End;
	for (REGEX_NAMESPACE::sregex_iterator i(response.begin(), response.end(), pattern); i != End; ++i)	{
		if ((*i).size() < 3)
			continue; //failsafe
		NString tld = (*i)[1].str();
		NString desc = (*i)[2].str();
		tldmap[tld.Trim_r().asLower()] = desc.Trim_r();
	}
}

void tld_plugin::OnConnected(IRCSession *session) {
	this->session = session;
}
void tld_plugin::OnLoad(IRCSession *session) {
	this->session = session;
	ParseTlds();
}

MODULE_EXPORT CPlugin *getModule() {
	CPlugin *module = new tld_plugin();
	module->setInfo("tld", "returns the land of a tld [!tld .de]");
	return module;
}