#include <iostream>
#include <map>

#include "CPlugin.h"
#include "CIRCMessage.h"
#include "CIRCSession.h"
#include "NString.h"

class tld_plugin : public CPlugin {
private:
	std::map<NString, NString> tldmap;
	IRCSession *session;
	void ParseTlds();
public:
	void OnConnected(IRCSession *session);
	void OnPrivMessage(IRCChannel *chan, IRCUser *user, IRCMessage * msg);
	void OnLoad(IRCSession *session);
};