SOURCE_GROUP("Header\ Files\\Socket" FILES 
				src/include/NSock.h
)

SOURCE_GROUP("Header\ Files\\Commands" FILES
				src/include/ChatCommands.h
)

SOURCE_GROUP("Header\ Files\\Events" FILES
				src/include/EventMgr.h
				src/include/events.h
)

SOURCE_GROUP("Header\ Files\\Core" FILES
				src/include/main.h
				src/include/CIRCChannel.h
				src/include/CIRCUser.h
				src/include/CIRCMessage.h
				src/include/CIRCCommand.h
				src/include/CIRCSession.h
				src/include/defines.h
				src/include/CConfig.h
				src/include/NString.h
				src/include/Threads.h
				src/include/CSemaphore.h
				src/include/CPluginManager.h
				src/include/CPlugin.h
				src/include/CQueue.hpp
)

SOURCE_GROUP("Source\ Files\\Socket" FILES 
				src/NSock.cpp
)

SOURCE_GROUP("Source\ Files\\Commands" FILES
				src/ChatCommands.cpp
)

SOURCE_GROUP("Source\ Files\\Events" FILES
				src/EventMgr.cpp
				src/events.cpp
)	
			
SOURCE_GROUP("Source\ Files\\Core" FILES
				src/main.cpp
				src/CIRCChannel.cpp
				src/CIRCUser.cpp
				src/CIRCMessage.cpp
				src/CIRCSession.cpp
				src/NetworkThread.cpp
				src/Queues.cpp
				src/CConfig.cpp
				src/NString.cpp
				src/Threads.cpp
				src/CSemaphore.cpp
				src/CPluginManager.cpp
)