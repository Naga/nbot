IF (UNIX)
	ADD_CUSTOM_TARGET (distclean)
	SET(DISTCLEANED
		cmake.depends
		cmake.check_depends
		CMakeCache.txt
		cmake.check_cache
		CMakeFiles
		src/CMakeFiles
		*.cmake
		Makefile
		src/*.o
		NBot
		*~
	)
  
	ADD_CUSTOM_COMMAND(
		DEPENDS clean
		COMMENT "distribution clean"
		COMMAND rm
		ARGS -Rf CMakeTmp ${DISTCLEANED}
		TARGET  distclean
	)
ENDIF(UNIX)