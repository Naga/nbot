#include "include/Threads.h"
#include "include/CSemaphore.h"
#include "include/CIRCSession.h"



void SendThread(void *param) {
	IRCSession* irc = static_cast<IRCSession*>(param);
	while (!irc->isDisconnecting()) {
		irc->send_semaphore->wait();
		while(irc->out_queue.size() > 0)
			irc->ProcessSendQueue();
	}
}

void RecvThread(void *param) {
	IRCSession* irc = static_cast<IRCSession*>(param);
	CServerConfig *config = irc->getConfig();
	while (1) {

		if (irc->isDisconnecting())
			return;
	
		if (!irc->getSocket()->Connected()) {
			usleep(50); // give the process_in_queue time to show the message, just style related
			irc->Disconnect("Socket Closed",true, true);
			return;
		}
		
		int avail = irc->getSocket()->available();
		if (avail == -1) {
			irc->Disconnect("Socket Error",true, true);
			return;
		}
		NString ret;
		irc->getSocket()->RecvUntil(ret, "\r\n");
		if (ret.empty()) continue;

		for(unsigned short int i = 0; i <=2; ++i)
			if (ret.length() > 2 && (ret.substr(ret.length() -2) == "\r\n"))
				ret = ret.substr(0, ret.length()-2);

		irc->in_queue.push_back(ret);
		irc->process_semaphore->signal();
	}
}

void ProcessThread(void *param) {
	IRCSession* irc = static_cast<IRCSession*>(param);
	while (!irc->isDisconnecting()) {
		irc->process_semaphore->wait();
		while(irc->in_queue.size() > 0)
			irc->ProcessRecvQueue();
	}
}
