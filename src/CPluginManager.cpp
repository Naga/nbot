#include "include/CPluginManager.h"
#include "include/EventMgr.h"
bool CPluginManager::instance_exists = false;
CPluginManager* CPluginManager::instance = NULL;

NString get_libload_error() {
		NString error;
#ifdef WINDOWS
		char buf[256];
		FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL, GetLastError(), MAKELANGID(LANG_NEUTRAL,SUBLANG_DEFAULT), buf, strlen(buf)-1, NULL);
		error = buf;
#else
		error = dlerror();
#endif
		return "ERROR Loading Library: "+error;
}


bool CPluginManager::Load(IRCSession* session, NString path) {
#ifdef WINDOWS
	std::cout << "Plugin loading disabled in windows" << std::endl;
	return false;
#endif


	MODHANDLE modh;
	CPlugin *module;
	last_error = "";
	std::cout << "Loading Library " << path << std::endl;
#ifdef WINDOWS
	if ((modh = LoadLibrary(path.c_str())) != NULL) {
		MOD_INIT_FUNC_HANDLE function;
		if ((function = GetProcAddress(modh, "getModule")) != (MOD_INIT_FUNC_HANDLE)NULL) {
#else
	if ((modh = dlopen(path.c_str(), RTLD_NOW)) != NULL) {
		MOD_INIT_FUNC_HANDLE function;
		if ((function = dlsym(modh, "getModule")) != (MOD_INIT_FUNC_HANDLE)NULL) {
#endif
			module = reinterpret_cast<CPlugin *(*)()>(function)();
			if (module) {
				if (plugins.find(session) == plugins.end())
					plugins[session] = new std::vector<plugin_info*>;

				if (findPlugin(session, module->getName()))
					return false; //already loaded

				plugin_info * plugin = new plugin_info(modh, module->getName(), module);
				module->OnLoad(session);
				plugins[session]->push_back(plugin);
			}
			return true;
		} else {
			last_error = get_libload_error();
		}
	} else {
		last_error = get_libload_error();
	}

	return false;
}

bool CPluginManager::Unload(IRCSession* session, NString name) {
	if (plugin_info * plugin = findPlugin(session, name)) {
		FREE_PLUGIN(plugin->modh);
		dlclose(plugin->modh);
		plugins[session]->erase(std::find(plugins[session]->begin(), plugins[session]->end(), plugin));
		delete plugin->plugin;
		delete plugin;
		return true;
	} else {
		return false;
	}
}

plugin_info* CPluginManager::findPlugin(IRCSession *session, NString name) {
	if (plugins.find(session) == plugins.end())
		return NULL;

	std::vector<plugin_info*> *plugs = plugins[session];
	
	for(std::vector<plugin_info*>::iterator it = plugs->begin(); it != plugs->end(); ++it) {
		if ((*it)->name == name)
			return (*it);
	}
		return NULL;
}

void CPluginManager::notify(Event *evt) {
	if(plugins.find(evt->session) == plugins.end())
		return;

	std::vector<plugin_info*>* vec = plugins[evt->session];

	for (std::vector<plugin_info*>::iterator it = vec->begin(); it != vec->end(); ++it)
		if (CPlugin *mod = (*it)->plugin)
			switch (evt->event_type) {
				case EVENT_CONNECTED:
					break; //do we need to inform the plugins that we successfully created a socket?
				case EVENT_CONNECTED2:
					mod->OnConnected(evt->session);
					break;
				case EVENT_NICKSERV:
					mod->OnNickServ(evt->msg);
					break;
				case EVENT_PRIVMSG:
					mod->OnPrivMessage(evt->chan, evt->user, evt->msg);
					break;
				case EVENT_ONJOIN:
					mod->OnJoin(evt->chan, evt->user);
					break;
				case EVENT_ONNICKCHANGE:
					mod->OnNickChange(evt->user, evt->msg);
					break;
				case EVENT_ONQUERY:
					mod->OnQuery(evt->user, evt->msg);
					break;
				case EVENT_ONKICK:
					mod->OnKick(evt->chan, evt->user, evt->msg);
					break;
				case EVENT_ONPART:
					mod->OnPart(evt->chan, evt->user, evt->msg);
					break;
				case EVENT_ONQUIT:
					mod->OnQuit(evt->chan, evt->user, evt->msg);
					break;
				case EBENT_ONMODECHANGE:
					mod->OnModeChange(evt->chan, evt->user, evt->msg);
					break;
				case EVENT_ONBAN:
					mod->OnBan(evt->chan, evt->user, evt->msg);
					break;
				case EVENT_ONUNBAN:
					mod->OnUnBan(evt->chan, evt->user, evt->msg);
					break;
				case EVENT_ONINVITE:
					mod->OnInvite(evt->chan, evt->user);
					break;
				case EVENT_ONERROR:
					mod->OnError(evt->msg);
					break;
				case EVENT_ONKILL:
					mod->OnKill(evt->msg);
					break;
				case EVENT_ONPING:
					mod->OnPing(evt->msg);
					break;
				case EVENT_CTCP:
					mod->OnCTCP(evt->chan, evt->user, evt->msg);
					break;
				case EVENT_CTCP_ACTION:
					mod->OnCTCPAction(evt->chan, evt->user, evt->msg);
					break;
				default:
					std::cout << "[ModuleManager]: Unhandled event type " << evt->event_type << std::endl;
			}
}

CPluginManager * CPluginManager::getInstance() {
	if(!instance_exists) {
		instance = new CPluginManager();
		instance_exists = true;
	}
		return instance;
}

