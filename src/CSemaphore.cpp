
#include "include/CSemaphore.h"

void CSemaphore::wait() {
	std::unique_lock<std::mutex> lk(m_);
	cv_.wait(lk, [=]{ return 0 < count_; });
	--count_;
}

bool CSemaphore::try_wait() {
	std::lock_guard<std::mutex> lk(m_);
	if (0 < count_) {
		--count_;
		return true;
	} else {
		return false;
	}
}

void CSemaphore::signal() {
	std::lock_guard<std::mutex> lk(m_);
	if (count_ < count_max) {
		++count_;
		cv_.notify_one();
	}
}
