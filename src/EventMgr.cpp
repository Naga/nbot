#include "include/EventMgr.h"
#include "include/CIRCMessage.h"
#if defined(HAS_STD_REGEX)
	#include <regex>
#elif defined(HAS_BOOST)
	#include "boost/xpressive/xpressive.hpp"
#endif

bool EventMgr::instance_exists = false;
EventMgr* EventMgr::instance = NULL;

void EventMgr::fire(EventType event_type, IRCMessage* msg,IRCChannel* chan, IRCUser* user) {
	if (callbacks.find(event_type) == callbacks.end() && 
		callbacks.find(EVENT_ALL) == callbacks.end() && 
		rxcallbacks.find(EVENT_ALL) == rxcallbacks.end() &&
		plugincallbacks.find(EVENT_ALL) == plugincallbacks.end())
		return;

	for (std::vector<callback>::iterator it = callbacks[EVENT_ALL].begin(); it != callbacks[EVENT_ALL].end(); ++it) {
		Event *ev = new Event(network, msg, chan, user);
		ev->event_type = event_type;
		(*it)(ev);
	}

	for (std::vector<callback>::iterator it = callbacks[event_type].begin(); it != callbacks[event_type].end(); ++it) {
		Event *ev = new Event(network, msg, chan, user);
		ev->event_type = event_type;
		(*it)(ev);
	}

	for (std::vector<plugin_callback_set*>::iterator it = plugincallbacks[event_type].begin(); it != plugincallbacks[event_type].end(); ++it) {
		Event *ev = new Event(network, msg, chan, user);
		ev->event_type = event_type;
		plugin_callback_set *pset = (*it);
		(pset->func)(pset->plugin,ev);
	}
	
#if defined(HAS_STD_REGEX) || defined(HAS_BOOST)
	for (std::vector<std::pair<NString,callback>>::iterator it = rxcallbacks[event_type].begin(); it != rxcallbacks[event_type].end(); ++it) {
		NString regexpattern = it->first;
		callback func = it->second;
		
#if defined(HAS_STD_REGEX)
		std::regex pattern = std::regex(regexpattern);
		std::smatch matches;
		bool matched = std::regex_match(msg->message, matches, pattern);
#elif defined(HAS_BOOST)
		boost::xpressive::sregex pattern = boost::xpressive::sregex::compile(regexpattern);
		boost::xpressive::smatch matches;
		bool matched = boost::xpressive::regex_match(msg->message, matches, pattern);
#endif	
		if (!matched) {
			return;
		}
		
		
		Event *ev = new Event(network, msg, chan, user);
		ev->event_type = event_type;
		(func)(ev);
	}
#endif
}

EventMgr* EventMgr::getInstance() {
    if(!instance_exists) {
        instance = new EventMgr();
        instance_exists = true;
	}
        return instance;
}

bool EventMgr::RegisterCallback(int type, callback func) {
	if (std::find(callbacks[type].begin(), callbacks[type].end(), func) == callbacks[type].end()) {
		callbacks[type].push_back(func);
		return true;
	}
	return false;	
}

bool EventMgr::RegisterPluginCallback(int type, CPlugin* plugin, plugincallback func) {
	plugin_callback_set* pset = new plugin_callback_set(plugin, func);
	if (std::find(plugincallbacks[type].begin(), plugincallbacks[type].end(), pset) == plugincallbacks[type].end()) {
		plugincallbacks[type].push_back(pset);
		return true;
	}
	//else
	delete pset;
	return false;
}

bool EventMgr::RegisterRegexMsgCallback(EventType type, NString regex, callback func) {
	if (std::find(rxcallbacks[type].begin(), rxcallbacks[type].end(), std::pair<NString,callback>(regex,func)) == rxcallbacks[type].end()) {
		rxcallbacks[type].push_back(std::pair<NString,callback>(regex,func));
		return true;
	}
	return false;
}

void EventMgr::SetSession(IRCSession* net) {
	network = net;
}