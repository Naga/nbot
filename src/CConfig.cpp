#include "include/CConfig.h"
#include <iostream>


CConfigMgr* CConfigMgr::instance = NULL;


CConfigMgr* CConfigMgr::getInstance() {
    if(!instance)
        instance = new CConfigMgr();

    return instance;
}

void CConfig::add(NString key, NString val) {
	confmap.insert(std::make_pair(key, val));
}

void CConfigMgr::addConfig(NString server, CServerConfig* config) {
	config->add("server_name", server);
	confmap[server] = config;
}

void CServerConfig::addPluginConfig(NString pluginname, CPluginConfig * plugin) {
	plugin->add("plugin_name", pluginname);
	plugin_confmap[pluginname] = plugin;
}


CServerConfig * CConfigMgr::getServerConfig(NString servername) {
	std::map<NString, CServerConfig*>::iterator it = confmap.find(servername);
	return (it != confmap.end()) ? it->second : (CServerConfig*)0;
}

CPluginConfig * CServerConfig::getPluginConfig(NString pluginname) {
	std::map<NString, CPluginConfig*>::iterator it = plugin_confmap.find(pluginname);
	return (it != plugin_confmap.end()) ? it->second : (CPluginConfig*)0;
}

bool CConfigMgr::LoadFile(NString strfilename, bool reload) {
	if (reload)
		confmap.clear();

	filename = strfilename;
	std::ifstream  cfile;
	cfile.open(filename.c_str());

	if (!cfile) {
		return false;
	}


	//vars
	unsigned int linenum = 0;
	NString current_server("");
	NString current_plugin("");
	NString current_tag("");
	std::vector<NString> open_tags;
	std::map<NString, NString> config;
	std::map<NString, NString> plugin_config;
	std::map<NString, CPluginConfig*> plugin_config_map;

	while (cfile.good()) {
		NString line, val;
		NString key;
		++linenum;

		getline(cfile, line);
		if (line.empty() || line[0] == '#' || line[0] == ';' || (line.find("=") == std::string::npos && line.find("<") == std::string::npos)) //not a valid line
			continue;

		line.Trim();

		if (line.Left(1) == "<" && line.Right(1) == ">") {// we got a tag here
			NString tmpline = line.asLower();

			if (tmpline.Left(2) == "</") { //closing tag
				tmpline.cut_right(1);
				tmpline.cut_left(2);
				NString tag = tmpline.Token(0);

				if (open_tags.size() == 0) {
					std::cout << "Error parsing config file: close tag " << tag << " found but no tag is currently open at line: " << linenum << std::endl;
					exit(1);
				}

				if (tag == "server") {
					CServerConfig *cfg = new CServerConfig(config);
					
					for(std::map<NString, CPluginConfig*>::iterator it = plugin_config_map.begin(); it != plugin_config_map.end(); ++it) {
						cfg->addPluginConfig(it->first, it->second);
					}

					addConfig(current_server, cfg);
					current_server = "";
					config.clear();
				}
				if (tag == "plugin") {
					CPluginConfig *plugin = new CPluginConfig(plugin_config);
					plugin_config_map[current_plugin] = plugin;
					current_plugin = "";
					plugin_config.clear();
				}

				open_tags.pop_back();

				current_tag = (open_tags.size() > 0) ? open_tags[open_tags.size()-1] : "";

				//cleanup
				continue;
			} else { //opening tag
				tmpline.cut_right(1);
				tmpline.cut_left(1);
				NString tag = tmpline.Token(0);

				if (tag == "server") {
					if (open_tags.size() > 0  && current_tag == "server") {
						std::cout << "Error parsing config file: unclosed server tag while opening a new one at line: " << linenum << std::endl;
						exit(1);
					}
					current_server = tmpline.Token(1);
					if (current_server.empty() ) {
						std::cout << "Error opening server tag without giving server name at line: " << (int)linenum << std::endl;
						exit(1);
					}
					
				}
				if (tag == "plugin") {
					current_plugin = tmpline.Token(1);
				}
				//++open_tags;
				open_tags.push_back(tag);
				continue;
			}
		}

		//key parsing

		key = line.substr(0, line.find_first_of("="));
		key.toLower().Trim();

		if (line.find(";") != std::string::npos) {
			unsigned int start = line.find_first_of("=")+1;
			unsigned int end = line.find_first_of(";");
			val = line.substr(start, end-start).Trim_r();
		} else
			val = line.substr(line.find_first_of("=")+1, line.length()).Trim_r();

		if (key.empty() || val.empty())
			continue;


		if ( open_tags[open_tags.size()-1] == "server")
			config[key] = val;
		else if (open_tags[open_tags.size()-1] == "plugin")
			plugin_config[key] = val;

	}


	if (reload)
		std::cout << "Reloaded config file: " << filename << std::endl;
	else
		std::cout << "Loaded config file: " << filename << std::endl;

	return true;
}

void CConfigMgr::Reload() {
	if (!LoadFile(filename, /*bool rehash =*/ true)) {
		std::cout << "Failed to reload file: server.cnf!" << std::endl;
		exit(1);
	}
}


bool CConfig::key_exists(NString key) {
	if (confmap.find(key) != confmap.end())
		return true;
	else
		return false;
}

