#include "include/CModule.h"
#include "include/CIRCMessage.h"
#include "include/CIRCSession.h"

class my_module : public CModule {
	IRCSession *session;
	void OnPing(IRCMessage *msg) {
		std::cout << "[module]: " << session->GetBotNick() << " Pinged me!" << std::endl;
	}
	void OnConnected(IRCSession *session) {
		this->session = session;
	}
};


MODULE_EXPORT CModule *getModule() {
	CModule *module = new my_module();
	module->setInfo("testmodule", "just a simple testmodule");
	return module;
}