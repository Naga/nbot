#include "include/CIRCChannel.h"
#include "include/CIRCUser.h"

void IRCChannel::AddUser(IRCUser* user) {
	if (userlist.find(name) != userlist.end())
		userlist.insert(std::pair<NString, IRCUser*>(user->getNick(), user));
}

void IRCChannel::DelUser(IRCUser* user) {
	std::map<NString, IRCUser*>::iterator it = userlist.find(user->getNick());
	if (it != userlist.end())
		userlist.erase(it);
}
void IRCChannel::DelUser(NString name) {
	std::map<NString, IRCUser*>::iterator it = userlist.find(name);
	if (it != userlist.end())
		userlist.erase(it);
}

bool IRCChannel::isUserOnChan(NString name) {
	return (userlist.find(name) != userlist.end());
}
