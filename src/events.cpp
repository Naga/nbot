#include "include/events.h"
#include "include/defines.h"
#include "include/CIRCSession.h"
#include "include/CIRCUser.h"
#include "include/CIRCChannel.h"
#include "include/CIRCMessage.h"

void callModuleManager(Event* evt) { //hacky proxy, better way?
	CPluginManager::getInstance()->notify(evt);
}

void JoinConfigChans(Event* evt ) {
	CServerConfig* config = evt->session->getConfig();
	std::vector<NString> channels;
	channels = config->GetString("channels").split_delim(",");
	for(std::vector<NString>::iterator it = channels.begin(); it != channels.end(); ++it)
		evt->session->JoinChannel((*it));
}

void SetDefaultModes(Event* evt) {
	CConfig* config = evt->session->getConfig();
	NString defaultmodes = config->GetString("default_bot_modes");

	//lets assume it's a well formatted mode string.
	evt->session->PutServ("MODE %s %s", evt->session->getBotNick().c_str(), defaultmodes.c_str());
}

void HandlePrivMsg(Event *evt) {
	CConfig * config = evt->session->getConfig();
	if (config->GetBoolean("ignore_bots") && evt->user->getModes().find("B") != std::string::npos)
		return;

	EventMgr *eventmgr = EventMgr::getInstance();		
		
    NString m = evt->msg->message;
    if (m[0] == '!') {  //custom triggers? TODO: implement
            NString cmd = m.substr(0, m.find(" "));
            if (evt->session->cmds.find(cmd) != evt->session->cmds.end()) {
                    evt->msg->message = m.substr(m.find(" ")+1, m.length());
                    evt->cmdinfo.cmdname = cmd;
                    evt->cmdinfo.trigger = "!";
                    (*evt->session->cmds[cmd])(evt);
            }
    }
}

void HandleCTCP(Event *evt) {
	NString message = evt->msg->message.substr(1, evt->msg->message.find_last_of('\001')-1);
	EventMgr * events = EventMgr::getInstance();
	if (message.find("ACTION") == 0)
		events->fire(EVENT_CTCP_ACTION, evt->msg, evt->chan, evt->user);
	else if (message.find("PING") == 0)
		evt->session->PutQuick("NOTICE %s :\001PING %s\001", evt->msg->sender.nick.c_str(), message.substr(message.find_last_of(" ")+1, message.length()).c_str());
	else if (message.find("VERSION") == 0)
		evt->session->PutServ("NOTICE %s :\001VERSION NBot v%d.%d.%d running on %s\001",evt->msg->sender.nick.c_str(),VER_MAJOR, VER_MINOR, VER_REV, OS_TYPE);
	else if (message.find("FINGER") == 0)
		evt->session->PutServ("NOTICE %s :\001FINGER Keep your filthy fingers off me!\001",evt->msg->sender.nick.c_str());
	else if (message.find("TIME") == 0) {
		time_t rawtime;
		time ( &rawtime );
		evt->session->PutServ("NOTICE %s :\001TIME %s\001",evt->msg->sender.nick.c_str(),ctime(&rawtime));
	} else
		evt->session->PutServ("NOTICE %s :Unknown CTCP: %s",evt->msg->sender.nick.c_str(),message.c_str());
}

void nickserv_identify(Event* evt) {
	CConfig *config = evt->session->getConfig();
	NString ns_name;

	if (config->GetString("nickserv_pass").empty())
		return;

	ns_name = (config->GetString("nickserv_name").empty() ? "nickserv" : config->GetString("nickserv_name"));
	evt->session->PutServ("PRIVMSG %s :IDENTIFY %s", ns_name.c_str(), config->GetString("nickserv_pass").c_str());
	return;
}