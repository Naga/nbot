#include "include/main.h"
#include "include/CIRCSession.h"
#include "include/EventMgr.h"
#include "ctype.h"
#include "include/defines.h"
std::vector<IRCSession *> sessions;

int main () {
#ifdef WINDOWS
	SetConsoleCtrlHandler((PHANDLER_ROUTINE)CtrlHandler, TRUE );
	setlocale(LC_ALL,"German");
#else
	// signal(SIGINT,  sighandler);
	// signal(SIGQUIT, sighandler);
	// signal(SIGABRT, sighandler);
	// signal(SIGHUP,  sighandler);
	// signal(SIGKILL, sighandler);
#endif

	CPluginManager *modmgr = CPluginManager::getInstance();
	CConfigMgr *configmgr = CConfigMgr::getInstance();


	if (!configmgr->LoadFile("server.conf")) {
		SendDebugOutput("Failed to load file: server.cnf!");
		exit(0);
	}


	for (std::map<NString, CServerConfig*>::iterator it = configmgr->confmap.begin(); it != configmgr->confmap.end(); ++it) {
		CServerConfig * conf = it->second;
		IRCSession* irc_s = new IRCSession(conf);
		
		if (!conf->GetBoolean("active"))
			continue;

		std::cout << "Connecting to: " << conf->GetString("server_name") << ":" << conf->GetString("port") << std::endl;


		for(auto plugpair: conf->getPluginMap()) {
			if (CPluginConfig *plugincfg = plugpair.second)
				if (!plugincfg->GetBoolean("active"))
					continue;

			NString name = MODULE_PREFIX+plugpair.first+MODULE_EXT;
			if (!modmgr->Load(irc_s, name))
				std::cout << modmgr->getLastError().c_str() << std::endl;
		}

		if (!irc_s->Connect()) {
				std::cout <<  "Error connecting to Server ( " << get_last_error() << " )" << std::endl;
				return false;
		}
		
		if (!irc_s->getSocket()->isSSL())
			std::cout << "Connected." << std::endl;
		else
			std::cout << "Connected with " << irc_s->getSocket()->get_used_chiper_version() << "-" << irc_s->getSocket()->get_used_chiper_name()  << std::endl;


		sessions.push_back(irc_s);
	}
	bool chatmode = false;
	NString mode("");
	NString chatchan;




	//WORKAROUND - console only works for the first server -> temp wontfix
	IRCSession* irc_s;
	if (sessions.size() == 0)
		exit(1);

	irc_s = sessions[0];

	while(1) {
		if (chatmode)
			std::cout << "Chat>";
		else
			std::cout << ">";


		NString out;
		NString str;
		std::getline( std::cin, str);

		if (std::cin.eof()) { //CRTL+D or CTRL+Z pressed, prevent endless loop or close
			std::cout << "EOF detected, to exit please enter \"exit\"" << std::endl;
			std::cin.clear();
			continue;
		}

		if (!irc_s->getConnectionState()) {
			std::cout << "\n\nConnection error! ENTER to exit";
			//break;
		}

		if (chatmode && str.find("chat") == 0) {
			chatmode = false;
			continue;
		} else if (!chatmode && str.find("rehash") == 0) {
			configmgr->Reload();
			continue;
		}

		if (chatmode && strlen(str.c_str()) > 1) {
			std::stringstream ss;
			ss << "PRIVMSG " << chatchan << " :" << str;
			irc_s->PutServ(ss.str().c_str());
			continue;
		}

		if (str.find("chat") == 0) {
			if (str.length() > str.find("#")) {
				chatchan = str.substr(str.find("#"), str.length());
			} else
				continue;


			if (chatchan.empty()) {
				std::cout << "Chat in which channel?" << std::endl;
				continue;
			}
			if (!irc_s->FindChannel(chatchan)) {
				std::cout << "I'm not on that channel!" << std::endl;
				continue;
			}
			chatmode = true;
			continue;
		}

		if (strcmp(str.c_str(),"exit") == 0) {
			irc_s->Disconnect("Terminating...", false, false);
			return true;
		}

		if (str.size() > 0)
			irc_s->PutServ(str);
	}
	std::cin.get();

return true;
}

void Shutdown() {
		for(std::vector<IRCSession*>::iterator it = sessions.begin(); it != sessions.end(); ++it) 
			if (IRCSession* is = *it)
				is->Disconnect("Sayonara!");
		exit(1);
}

#ifndef WINDOWS
void sighandler (int signal) {
	Shutdown();
}
#else
BOOL CtrlHandler(DWORD type) {
	if (type == CTRL_C_EVENT ||
		type == CTRL_CLOSE_EVENT ||
		type == CTRL_BREAK_EVENT ||
		type == CTRL_LOGOFF_EVENT ||
		type == CTRL_SHUTDOWN_EVENT) {
		Shutdown();
		return true;
	}
  return false;
}
#endif


void SendDebugOutput (NString out, ...) {
	va_list marker;
	char szBuf[1024];
	va_start(marker, out);
	vsprintf_s(szBuf, out.c_str(), marker);
	va_end(marker);
	out = szBuf;

#ifdef WINDOWS
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_SCREEN_BUFFER_INFO csbiInfo;
	GetConsoleScreenBufferInfo( hConsole, &csbiInfo );
	SetConsoleTextAttribute(hConsole, FOREGROUND_RED);
	std::cout << out << std::endl;
	SetConsoleTextAttribute(hConsole, csbiInfo.wAttributes);
#else
	std::cout << "\033[22;31m" << out << "\033[0m" << std::endl;
#endif
}

void SetDbgConsoleTitle(NString title, ...) {
	va_list marker;
	char buf[1024];
	
	va_start(marker, title);
	vsprintf_s(buf, title.c_str(), marker);
	va_end(marker);
	title = buf;
#ifdef LINUX
	std::cout << "\033]0;" << title.c_str() << "\007";
#elif defined(WINDOWS)
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTitle(title.c_str());
#endif
}

NString get_last_error(int error) {
#ifdef WINDOWS
	char cerror[1024];
	strerror_s(cerror, error);
	return cerror;
#else
	return strerror(error);
#endif
}

