#include "include/CIRCSession.h"
#include "include/CIRCChannel.h"
#include "include/CIRCUser.h"
#include "include/CIRCCommand.h"
#include "include/CIRCMessage.h"
#include "include/EventMgr.h"
#include "include/defines.h"
#include "include/CConfig.h"
#include "include/CSemaphore.h"
#include <iterator>

NString IRCSession::getServerName() {
	return server;
}

NSocket* IRCSession::getSocket() {
	return m_socket;
}
bool IRCSession::Connect(bool reconnect) {
	disconnecting = false;
	m_socket = new NSocket(server.c_str(), port, useSSL, ipv6_enabled);

	if (reconnect)
		++reconnect_try;

	if (!m_socket->start()) {
		std::cout << "Cannot connect to " << server.c_str() << ":" << port << std::endl;

		if (reconnect && reconnect_try >= max_reconnect_trys) {
			std::cerr << "Max reconnect trys reached, giving up" << std::endl;
			exit(1);
		}

		if (!reconnect)
			exit(1);
		else
			return false;
	}


	EventMgr* eventmgr = EventMgr::getInstance();
	CPluginManager *modmgr = CPluginManager::getInstance();


	eventmgr->SetSession(this);

	eventmgr->RegisterCallback(EVENT_ALL, callModuleManager);
	eventmgr->RegisterCallback(EVENT_CONNECTED2, JoinConfigChans);
	eventmgr->RegisterCallback(EVENT_CONNECTED2, SetDefaultModes);
	eventmgr->RegisterCallback(EVENT_CONNECTED2, RegisterCommands);
	eventmgr->RegisterCallback(EVENT_CTCP, HandleCTCP);
	eventmgr->RegisterCallback(EVENT_NICKSERV, nickserv_identify);


	eventmgr->RegisterCallback(EVENT_PRIVMSG, HandlePrivMsg);

	send_semaphore = new CSemaphore();
	process_semaphore = new CSemaphore();

	recv_thread = new std::thread(RecvThread, this);
	send_thread = new std::thread(SendThread, this);
	process_thread = new std::thread(ProcessThread, this);

	if(!password.empty())
		PutServ("PASS %s", password.c_str());

	PutServ("NICK %s", botnick.c_str());

	nick_trys = 0;
	PutServ("USER %s 0 * :%s", botnick.c_str(), realname.c_str());
	return true;
}

IRCSession::IRCSession(CServerConfig * conf) {
	this->server = conf->getServerName();
	this->port = conf->GetInt("port");
	this->botnick = conf->GetString("botnick");
	this->realname = conf->GetString("realname");
	this->password = conf->GetString("password");
	this->useSSL = conf->GetBoolean("ssl");
	this->ipv6_enabled = conf->GetBoolean("ipv6");
	this->connected = false;
	this->config = conf;

	max_reconnect_trys = 10;
	reconnect_try = 0;
}

bool IRCSession::JoinChannel (NString channel) {
	if (!FindChannel(channel)) {
		PutServ("JOIN %s",channel.c_str());
		return true;
	} else
		return false;
}

bool IRCSession::LeaveChannel (NString channel, NString message) {
	if (IRCChannel *chan = FindChannel(channel)) {
		PutServ("PART %s %s",channel.c_str(), message.c_str());
		chans.erase(std::find(chans.begin(), chans.end(), chan));
		return true;
	} else
		return false;
}

bool IRCSession::ChangeNick (NString newNick) {
	PutServ("NICK %s",newNick.c_str());
	return true;
}


void IRCSession::Disconnect(NString qMsg, bool forced, bool reconnect) {
	std::cout << "Error: Disconnecting from " << getServerName() << " ... " << std::endl;
	disconnecting = true;

	if (getSocket()->Connected() && !forced) {
		NString str("QUIT :");
		str += qMsg;
		str += "\r\n";

		getSocket()->SendLine(str);
		getSocket()->Disconnect();
	}
	if (getSocket()->Connected() && forced) {
		getSocket()->Disconnect();
	}


	if (recv_thread) {
		recv_thread->detach();
	}
	if (send_thread) {
		send_semaphore->signal();
		send_thread->join();
	}

	if (process_thread) {
		process_semaphore->signal();
		process_thread->join();
	}

	delete process_thread;
	delete recv_thread;
	delete send_thread;
	delete send_semaphore;
	delete process_semaphore;

	for(auto chan: chans) {
		delete chan;
	}
	chans.clear();

	for(auto user: users) {
		delete user;
	}
	users.clear();

	connected = false;


	if (reconnect) {
		std::cout << "Reconnecting..." << std::endl;
		while(1) {
			if (!Connect(true)) {
				std::cerr << "Reconecting failed, wating 5 minutes" << std::endl;
				std::this_thread::sleep_for (std::chrono::seconds(10));
			} else
				break;
		}
	}
	disconnecting = false;
}

bool IRCSession::getConnectionState() {
	return connected;
}


bool IRCSession::ProcessIRCString(NString line) {
	if(line.empty())
		return false;
	EventMgr * events = EventMgr::getInstance();
	CServerConfig *config = getConfig();
	IRCMessage *msg = new IRCMessage();

	if (line[0] == ':') { //prefix exists
		NString pre_msg = line.substr(0, line.find(" :"));
		NString message = line.substr(line.find(" :")+2, line.length());

		msg->raw = line;
		msg->message = message;

		std::vector<NString> tokens;
		std::istringstream iss(pre_msg);
		std::copy(std::istream_iterator<NString>(iss), std::istream_iterator<NString>(), std::back_inserter<std::vector<NString> >(tokens));

		tokens[0].erase(0,1); //remove ':'


		if (tokens[0].find("@") != std::string::npos) {
			msg->sender.isServer = false;
			msg->sender.nick = tokens[0].substr(0, tokens[0].find_first_of("!"));
			msg->sender.user = tokens[0].substr(tokens[0].find_first_of("!")+1, tokens[0].find_first_of("@"));
			msg->sender.host = tokens[0].substr(tokens[0].find_first_of("@")+1, tokens[0].length());
		} else {
			msg->sender.isServer = true; // it only can be a servermessage here...right?
			msg->sender.host = tokens[0];
		}

		if (tokens.size() <= 1) {
			return false; //failsafe
		}
		msg->code = tokens[1];
		if (tokens[1].length() == 3) { //think we got a numeric code here

			switch (NString::toUInt(tokens[1])) {
			case 001: //welcome
				connected = true;
				events->fire(EVENT_CONNECTED);
				server = msg->sender.host;
			break;
			case 005: //server modes
				for (std::vector<NString>::const_iterator itr = tokens.begin(); itr != tokens.end(); ++itr) {
					if ((*itr).find("=") == std::string::npos)
						continue;
					NString option = (*itr).substr(0, (*itr).find_first_of("="));
					NString setting = (*itr).substr((*itr).find_first_of("=")+1, (*itr).length());

					if (option == "PREFIX") {
						NString chanmodes = setting.substr(setting.find_first_of("(")+1, setting.find_first_of(")")-1);
						NString chanmodechars = setting.substr(setting.find_first_of(")")+1, setting.length());
						serverModeMap["CHAN_MODES"] = chanmodes;
						serverModeMap["CHAN_MODE_CHARS"] = chanmodechars;
					}
					if (option == "CHANTYPES") {
						serverModeMap["chantypes"] = setting;
					}

					serverModeMap[setting] = option;
				}
				break;
			case 307: //WHOIS: is identified
				if (IRCUser* user = FindUser(tokens[3]))
					if (!message.empty())
						user->identified = true;
				break;
			case 311: // WHOIS info ident, host, realname etc...
				if (IRCUser* user = FindUser(tokens[3])) {
					user->host = tokens[5];
					user->ident = tokens[4];
					user->realname = message;
				}
				break;
			case 312: //WHOIS: server to which the user is connected
				if (IRCUser* user = FindUser(tokens[3]))
					user->server = tokens[4];
				break;
			case 319: //WHOIS: active channels of user
				//if we cant find the user here, something went wrong before
				if (IRCUser* user = FindUser(tokens[3])) {
					std::vector<NString> chans;
					std::istringstream iss(message);
					std::copy(std::istream_iterator<NString>(iss), std::istream_iterator<NString>(), std::back_inserter<std::vector<NString> >(chans));

					for (std::vector<NString>::const_iterator itr = chans.begin(); itr != chans.end(); ++itr) {
						NString channame = (*itr).substr((*itr).find_first_of("#"), (*itr).length()); //we will silently ignore chan modes here

						if (user->chanlist.find(channame) == user->chanlist.end())
							user->chanlist.insert(std::pair<NString, IRCChannel*>(channame, GetOrCreateChan(channame)));
					}
				}
				break;
			case 322: //LIST info (topic, usercount, modes)
				if (IRCChannel* chan = FindChannel(tokens[3])) {
					if (message.empty() || tokens.size() < 3 || tokens[3] == "*")
						break;

						if (message[0] == '[' && message.find_first_of("]") != std::string::npos) {
							chan->modes = message.substr(message.find_first_of("[")+1, message.find_first_of("]")-1);
							NString topic = message.substr(message.find_first_of("]")+1, message.length());
							chan->topic = topic;
						} else {
							chan->modes = "";
							NString topic = message.substr(0, message.length());
							chan->SetTopic(topic);
						}
						chan->userCount = (tokens.size() >= 5) ? NString::toUInt(tokens[4]) : 0;
				}
				break;
			case 331: //no topic set
				if (IRCChannel* chan = FindChannel(tokens[3]))
					chan->SetTopic("");
			break;
			case 332: //topic
				if (IRCChannel* chan = FindChannel(tokens[3]))
					chan->SetTopic(message);
				break;
			case 352: // WHO
				if (IRCUser *usr = GetOrCreateUser(tokens[7])) {
						if (usr->chanlist.find(tokens[3]) == usr->chanlist.end())
							usr->chanlist.insert(std::pair<NString, IRCChannel*>(tokens[3], GetOrCreateChan(tokens[3])));

						usr->ident = tokens[4];
						if (tokens[4].at(0) == '~') //need to store the information?
							tokens[4].cut_left(1);
						usr->host = tokens[5];
						NString modes;
						for(std::string::size_type i = 0; i < tokens[8].size(); ++i) {
							NString mode(tokens[8].at(i));
							mode.toLower();
							int pos = serverModeMap["CHAN_MODE_CHARS"].find(mode);
							if (pos == -1) {
								modes += tokens[8].at(i);
								continue;
							}
							modes += serverModeMap["CHAN_MODES"].at(pos);
						}
						usr->modes = modes;
						usr->server = tokens[6];
						usr->realname = message.substr(message.find_first_of(" ")).Trim_r();
				}
				break;
			case 353: //names
				if (IRCChannel *chan = GetOrCreateChan(tokens[4])) {
					PutServ("WHO %s", tokens[4].c_str()); //get users of the channel
					PutServ("LIST %s", tokens[4].c_str()); //get modes of the channel
					std::vector<NString> users;
					std::istringstream iss(message);
					std::copy(std::istream_iterator<NString>(iss), std::istream_iterator<NString>(), std::back_inserter<std::vector<NString> >(users));

					for (std::vector<NString>::const_iterator itr = users.begin(); itr != users.end(); ++itr) {
						NString username("");
						for (uint8_t i = 0; i <= serverModeMap["CHAN_MODE_CHARS"].length()-1; ++i)
							if ((*itr)[0] == serverModeMap["CHAN_MODE_CHARS"][i]) {
								username = (*itr).substr(1, (*itr).length());
								IRCUser* user = GetOrCreateUser(username);

								if (user->chanlist.find(tokens[4]) == user->chanlist.end())
									user->chanlist.insert(std::pair<NString, IRCChannel*>(chan->getName(), chan));

								if (chan->userlist.find(username) == chan->userlist.end())
									chan->userlist.insert(std::pair<NString, IRCUser*>(user->getNick(), user));

							}

						if (username.empty()) { //user has no modes set
							IRCUser* user = GetOrCreateUser((*itr));
							if (user->chanlist.find(tokens[4]) == user->chanlist.end())
									user->chanlist.insert(std::pair<NString, IRCChannel*>(chan->getName(), chan));
							if (chan->userlist.find(username) == chan->userlist.end())
									chan->userlist.insert(std::pair<NString, IRCUser*>(user->getNick(), user));
						}
					}
					chan->userCount = (uint8_t)chan->userlist.size();
				}
				break;
			case 376: // end of MOTD
			case 422: // MOTD not found
				nick_trys = 0;
				events->fire(EVENT_CONNECTED2);
				break;
			case 430: //cant change nickname
			case 432: //error in nickname
			case 433: { //nickname in use
					std::stringstream ss;
					++nick_trys;
					if (nick_trys > 1)
						ss << getBotNick().substr(0, getBotNick().length()-1) << nick_trys;
					else
						ss << getBotNick() << nick_trys;
					SetBotNick(ss.str());
					PutQuick("NICK %s", getBotNick().c_str());
				}
				break;
			case 451: //notregistered
				break;
			case 474: //ERR_BANNEDFROMCHAN
				//:server 474 botname #chan :Cannot join channel (+b)
				break;
			default:
				break;
			}
		} else {
			if (msg->code == "NOTICE") {
				if(msg->message.find("Dieser Nickname ist registriert") != std::string::npos)
					events->fire(EVENT_NICKSERV, msg);
			}
			if (msg->code == "MODE") {
				/*
				*0 = sender name
				*1 = MODE
				*2 = reciever name (chan/user)
				*3 = modestring
				*4 = targets / arguments
				*/

				//first char of reciever not a chan mode -> user
				bool usermode = serverModeMap["chantypes"].find(tokens[2].at(0)) == std::string::npos;

				if (usermode) { //user mode change
					NString modechange = message;

					bool take = false;
					unsigned int count = 1;
					for(std::string::iterator it = modechange.begin(); it != modechange.end(); ++it) {
						if (*it == '-') {
							take = true;
							continue;
						} else if (*it == '+') {
							take = false;
							continue;
						} else { //modechar here
							if (tokens.size() < 3+count) //failsafe, there should not be more modes than given users
								break;

							if (*it == 'b') {
									IRCChannel *chan = FindChannel(tokens[2]);
									IRCUser *user = FindUserByMask(tokens[4]);
									if (take) {
										events->fire(EVENT_ONUNBAN, msg, chan, user);
									} else {
										events->fire(EVENT_ONBAN, msg, chan, user);
									}
								continue;
							}


							NString user = tokens[3+count];
							if (IRCUser *usr = FindUser(user)) {
								int pos = usr->modes.find(*it);
								if (take && pos != std::string::npos)
									usr->modes.erase(usr->modes.find(*it));
								else if (!take && pos == std::string::npos)
									usr->modes += *it;
							}
							++count;
						}
					}
				} else { //chan mode change ? anything other possible?
					NString modechange = tokens[3];
					bool take = false;
					if (IRCChannel *chan = FindChannel(tokens[2])) {
						for(std::string::iterator it = modechange.begin(); it != modechange.end(); ++it) {
							if (*it == '-') {
								take = true;
								continue;
							} else if (*it == '+') {
								take = false;
								continue;
							} else { //modechar here
								int pos = chan->modes.find(*it);
								if (take && pos != std::string::npos)
									chan->modes.erase(chan->modes.find(*it));
								else if (!take && pos == std::string::npos)
									chan->modes += *it;
							}
						}
					}
				}
			}
			else if (msg->code == "NICK") {
				if(IRCUser* user = FindUser(msg->sender.nick)) {
					user->nick = message;
					PutQuick("WHO %s", user->nick.c_str()); //update user
					events->fire(EVENT_ONNICKCHANGE, msg, (IRCChannel*)0, user);
				} else
					events->fire(EVENT_ONNICKCHANGE, msg);
			}
			else if (msg->code == "JOIN") {
				if (msg->sender.nick == getBotNick())
					GetOrCreateChan(message);
				if (IRCChannel* chan = FindChannel(message)) {
					chan->AddUser(GetOrCreateUser(msg->sender.nick));
					events->fire(EVENT_ONJOIN, msg, chan);
				} else
					events->fire(EVENT_ONJOIN, msg);
			}
			else if (msg->code == "QUIT") {
				if (IRCUser *user = FindUser(msg->sender.nick)) { //needed for event?
					events->fire(EVENT_ONQUIT, msg, 0, user);

					for(std::map<NString, IRCChannel*>::iterator it = user->chanlist.begin(); it != user->chanlist.end(); ++it)
						if (IRCChannel *chan = it->second)
							chan->DelUser(user);

					users.erase(std::remove(users.begin(), users.end(), user), users.end());
					delete user;
				} else
					events->fire(EVENT_ONQUIT, msg);
			}
			else if (msg->code == "PART") {
				IRCUser *user = FindUser(msg->sender.nick); //needed for event?
				if (IRCChannel* chan = FindChannel(tokens[2])) {
					chan->DelUser(msg->sender.nick);
					events->fire(EVENT_ONPART, msg, chan, user);
				} else
					events->fire(EVENT_ONPART, msg, 0, user);
			}
			else if (msg->code == "KICK") {
				IRCUser *user = FindUser(tokens[3]);
				if (IRCChannel* chan = FindChannel(tokens[2])) {
					if (user->getNick() == getBotNick()) {
						chans.erase(std::find(chans.begin(), chans.end(), chan), chans.end());
						events->fire(EVENT_ONKICK, msg, (IRCChannel*)0, user);
					} else {
						chan->DelUser(tokens[3]);
						events->fire(EVENT_ONKICK, msg, chan, user);
					}
				} else
					events->fire(EVENT_ONKICK, msg, 0, user);
			}
			else if (msg->code == "TOPIC") {
				if( IRCChannel* chan = FindChannel(tokens[2])) {
					chan->SetTopic(message);
				}
			}
			else if (msg->code == "PRIVMSG") {
				IRCUser* user = GetOrCreateUser(msg->sender.nick);
				if (message.find_first_of('\001') == 0) {
						IRCChannel* chan = FindChannel(tokens[2]);
						events->fire(EVENT_CTCP, msg, chan, user);
				} else {
					if (tokens[2].Left(1) == "#") { //channel msg
						IRCChannel* chan = FindChannel(tokens[2]);
						events->fire(EVENT_PRIVMSG, msg, chan, user);
					} else if (tokens[2] == getBotNick()) { //priv query
						events->fire(EVENT_ONQUERY, msg, (IRCChannel*)0, user);
					}
				}
			}
			else if (msg->code == "INVITE") {
				if (config->GetString("bot_owner") == msg->sender.nick) {
					JoinChannel(message);
					if (IRCChannel *chan = GetOrCreateChan(message)) {
						events->fire(EVENT_ONINVITE, msg, chan, FindUser(botnick));
					} else
						events->fire(EVENT_ONINVITE, msg);
				} else
					PutServ("NOTICE %s :I'll only follow my master, dont waste my time you little insect.", msg->sender.nick.c_str());
			}
		}
	} else {
		std::vector<NString> tokens;
		std::istringstream ss(line);
		NString token;

		while(ss >> token) {
			tokens.push_back(token);
		}

		if(tokens[0] == "PING") {
			tokens[1].erase(0,1); //remove leading ':'
			PutQuick("PONG :%s", tokens[1].c_str());
			events->fire(EVENT_ONPING, msg);
		}
		if(tokens[0] == "ERROR") {
			tokens[1].cut_left(1); //remove leading ':'
			tokens[2].cut_right(1);

			if (tokens[1].asLower() == "closing" && tokens[2].asLower() == "link") {
				if (!disconnecting)
					Disconnect("", true, true);
			}

			events->fire(EVENT_ONERROR, msg);
		}
	}
	delete msg;
	return true;
}


IRCChannel* IRCSession::GetOrCreateChan(NString name) {
	if (name.empty())
		return (IRCChannel*)0;

	if (IRCChannel *chan = FindChannel(name.asLower())) {
		return chan;
	} else {
		IRCChannel *newchan = new IRCChannel(name.asLower());
		this->chans.push_back(newchan);
		return newchan;
	}
}

IRCUser* IRCSession::GetOrCreateUser(NString name) {
	if (name.empty())
		return (IRCUser*)0;

	if (IRCUser *user = FindUser(name)) {
		return user;
	} else {
		IRCUser *newuser = new IRCUser(name);
		this->users.push_back(newuser);
		return newuser;
	}
}

IRCChannel* IRCSession::FindChannel(NString name) {
	std::vector<IRCChannel*>::iterator itr;
	for (itr = chans.begin(); itr != chans.end(); ++itr)
		if ((*itr)->name == name.asLower())
			return (*itr);
	return (IRCChannel*)0;
}

IRCUser* IRCSession::FindUser(NString name) {
	std::vector<IRCUser*>::iterator itr;
	for (itr = users.begin(); itr != users.end(); ++itr)
		if ((*itr)->nick == name)
			return (*itr);
	return (IRCUser*)0;
}

IRCUser* IRCSession::FindUserByMask(NString mask) {
	NString nick;
	NString ident;
	NString hostmask;
	int pos, pos2;

	pos = mask.find_first_of("!");
	pos2 = mask.find_first_of("@");

	if (pos == std::string::npos || pos2 == std::string::npos)
		return (IRCUser*)0; //failsafe

	nick = mask.substr(0, pos);
	ident = mask.substr(pos+1, pos2-1-pos);
	hostmask = mask.substr(pos2+1, std::string::npos);

	if (ident.empty())
		ident = "*";
	if (nick.empty())
		nick = "*";
	if (hostmask.empty())
		hostmask = "*";


	std::vector<IRCUser*>::iterator itr;
	for (itr = users.begin(); itr != users.end(); ++itr)
		if ((*itr)->getIdent().WildCmp(ident) && (*itr)->getNick().WildCmp(nick) && (*itr)->getHost().WildCmp(hostmask))
			return (*itr);
	return (IRCUser*)0;
}

void IRCSession::PutQuick(NString line, ...) {
	va_list marker;
	char buf[1024];

	va_start(marker, line);
	vsprintf_s(buf, line.c_str(), marker);
	va_end(marker);
	line = buf;
	out_queue.push_front(buf);
	send_semaphore->signal();
}

void IRCSession::PutServ(NString line, ...) {
	if (line == "")
		return;
	va_list marker;
	char buf[1024];



	va_start(marker, line);
	vsprintf_s(buf, line.c_str(), marker);
	va_end(marker);
	line = buf;
	out_queue.push_back(buf);

	send_semaphore->signal();
}

void IRCSession::registerChatCmd(NString trigger, NString cmd, callback func) {
	IRCCommandInfo *Ccmd = new IRCCommandInfo(trigger.c_str(), cmd);
	this->cmds.insert(std::pair<NString, callback>(trigger+cmd, func));
}
