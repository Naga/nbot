#include "include/NString.h"


NString::NString(char c) : std::string() { std::stringstream s; s << c; *this = s.str(); }
NString::NString(unsigned char c) : std::string() { std::stringstream s; s << c; *this = s.str(); }
NString::NString(short i) : std::string() { std::stringstream s; s << i; *this = s.str(); }
NString::NString(unsigned short i) : std::string() { std::stringstream s; s << i; *this = s.str(); }
NString::NString(int i) : std::string() { std::stringstream s; s << i; *this = s.str(); }
NString::NString(unsigned int i) : std::string() { std::stringstream s; s << i; *this = s.str(); }
NString::NString(long i) : std::string() { std::stringstream s; s << i; *this = s.str(); }
NString::NString(unsigned long i) : std::string() { std::stringstream s; s << i; *this = s.str(); }
NString::NString(long long i) : std::string() { std::stringstream s; s << i; *this = s.str(); }
NString::NString(unsigned long long i) : std::string() { std::stringstream s; s << i; *this = s.str(); }
NString::NString(double i, int precision) : std::string() { std::stringstream s; s.precision(precision); s << std::fixed << i; *this = s.str(); }
NString::NString(float i, int precision) : std::string() { std::stringstream s; s.precision(precision); s << std::fixed << i; *this = s.str(); }

NString& NString::toUpper() {
	transform((*this).begin(), (*this).end(), (*this).begin(), static_cast<int (*)(int)>(toupper));
	return *this;
}

NString NString::asUpper() const {
	NString str = *this;
	transform(str.begin(), str.end(), str.begin(), static_cast<int (*)(int)>(toupper));
	return str;
}

NString NString::asLower() const {
	NString str = *this;
	transform(str.begin(), str.end(), str.begin(), static_cast<int (*)(int)>(tolower));
	return str;
}

NString& NString::toLower() {
	transform((*this).begin(), (*this).end(), (*this).begin(), static_cast<int (*)(int)>(tolower));
	return *this;
}
unsigned int StrtoUInt(NString str) {
	return strtoul(str.c_str(), (char**) NULL, 10);
}

unsigned int NString::toUInt() const {
	NString str = *this;
	return strtoul(str.c_str(), (char**)NULL, 10);
}

unsigned int NString::toUInt(NString str) {
	return strtoul(str.c_str(), (char**)NULL, 10);
}

NString NString::cut_to_char(NString src, char end) {
	if (src.find(end) != std::string::npos)
		src = src.substr(0, src.find_last_not_of(end)-1);
	return src;
}
void NString::cut_to_char(char end) {
	if ((*this).find(end) != std::string::npos)
		(*this) = (*this).substr(0, (*this).find_last_not_of(end)-1);
}

NString NString::Trim(const NString trim_chars) {
	TrimgRight();
	TrimLeft();
	return *this;
}


NString NString::TrimgRight(const NString trim_chars) {
	size_type i = find_last_not_of(trim_chars);

	if (i == this->length() -1)
		return *this;

	if (i == std::string::npos) {
		this->clear();
		return *this;
	}

	this->erase(i+1, npos);
	return *this;
}

NString NString::TrimLeft(const NString trim_chars) {
	size_type i = find_first_not_of(trim_chars);

	if (i == this->length() -1)
		return *this;

	if (i == std::string::npos) {
		this->clear();
		return *this;
	}

	this->erase(0, i);
	return *this;
}

NString NString::Trim_r(const NString trim_chars) {
	NString str = *this;
	str.TrimgRight(trim_chars);
	str.TrimLeft(trim_chars);
	return str;
}

NString NString::TrimgRight_r(const NString trim_chars) {
	NString str = *this;
	str.TrimgRight(trim_chars);
	return str;
}

NString NString::TrimLeft_r(const NString trim_chars) {
	NString str = *this;
	str.TrimLeft(trim_chars);
	return str;
}

NString NString::substr(size_type _Off, size_type _Count) const {
	std::string tmp = *this;
	return tmp.substr(_Off, _Count);
}

int NString::find_count(const char * search) {
	int count = 0;
	NString::size_type pos = (*this).find(search);
	while (pos != std::string::npos) {
		++count;
		pos = (*this).find(search, pos+1);
	}
	return count;
}

std::vector<NString> NString::split_delim(NString delim/*  = " "*/) {
	return split_delim_r(*this, delim);
}

std::vector<NString> NString::split_delim_r(NString source, NString delim/*  = " "*/) {
	std::vector<NString> strs;
	size_t last_offset = 0;

	for(;;) {
		size_t i = source.find_first_not_of(delim, last_offset);
		if (i == std::string::npos) {
			break;
		}

		size_t j = source.find_first_of(delim, i);
		if (j == std::string::npos) {
			strs.push_back( source.substr(i) );
			last_offset = source.length();
			break;
		}
		last_offset = j;
		strs.push_back( source.substr(i, j -i) );
	}
	return strs;
}

void NString::htmlEntitiesDecode () {
	NString subs[] = {
		"& #34;", "&quot;",
		"& #39;", "&apos;",
		"& #38;", "&amp;",
		"& #60;", "&lt;",
		"& #62;", "&gt;",
		"&34;", "&39;",
		"&38;", "&60;",
		"&62;"
	};
	
	NString reps[] = {
		"\"", "\"",
		"'", "'",
		"&", "&",
		"<", "<",
		">", ">",
		"\"", "'",
		"&", "<",
		">"
	};
	
	size_t found;
	for(int j = 0; j < 15; j++) {
		do {
			found = this->find(subs[j]);
	  		if (found != std::string::npos)
		    		this->replace (found,subs[j].length(),reps[j]);
    		} while (found != std::string::npos);
  	}


}

NString NString::Left(size_t num) const {
	num = (num > length()) ? length() : num;
	return substr(0, num);
}

NString NString::Right(size_t num) const {
	num = (num > length()) ? length() : num;
	return substr(length() - num, num);
}

NString NString::Token(size_t pos) {
	std::vector<NString> tokens =  split_delim(" ");
	if (tokens.size() > pos)
		return tokens[pos];
	else
		return "";
}

NString NString::cut_right(size_t len) {
	if ((this->length() - len) >= 0)
		this->erase(this->length()-len, npos);

	return *this;
}


NString NString::cut_left(size_t len) {
	if ((this->length() - len) >= 0)
		this->erase(0, len);

	return *this;
}


bool NString::WildCmp(const NString mask) {
	NString str = *this;
	return NString::WildCmp(mask, str);
}

bool NString::WildCmp(const NString mask, const NString str) {
  // Written by Jack Handy - jakkhandy@hotmail.com

	const char *wild = mask.c_str();
	const char *string = str.c_str();
	const char *cp = NULL, *mp = NULL;

	while ((*string) && (*wild != '*')) {
		if ((*wild != *string) && (*wild != '?')) {
			return false;
		}
		wild++;
		string++;
	}

	while (*string) {
		if (*wild == '*') {
			if (!*++wild) {
				return true;
			}
			mp = wild;
			cp = string+1;
		} else if ((*wild == *string) || (*wild == '?')) {
			wild++;
			string++;
		} else {
			wild = mp;
			string = cp++;
		}
	}

	while (*wild == '*') {
		wild++;
	}
	return (*wild == 0);
}