#ifndef _NBOT_THREADS_H
#define _NBOT_THREADS_H
void SendThread(void *param);
void RecvThread(void *param);
void ProcessThread(void *param);
#endif