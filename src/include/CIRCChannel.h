#include "NString.h"

#include <stdint.h>

class IRCUser;

#ifndef _CIRCCHANNEL_H
#define _CIRCCHANNEL_H

class IRCChannel {
	friend class IRCSession;
	protected:
		NString modes, topic, name;
		uint8_t userCount;
		std::map<NString, NString> UserModeMap;
		std::map<NString, IRCUser*> userlist; //<username, IRCChanne<username>>
	public:
		NString getName() {return name;}
		NString getTopic() {return topic;}
		NString getModes() {return modes;}
		unsigned int getUserCount() {return userCount;};

		bool isUserOnChan(NString name);
		bool changeMode(NString mode);
		bool changeTopic(NString topic);

		void AddUser(IRCUser* user);
		void DelUser(IRCUser* user);
		void DelUser(NString name);
		//setters
		void SetTopic(NString newtopic) {this->topic = newtopic;}
		void SetUserCount(uint8_t count) {this->userCount = count;}
		
		IRCChannel(NString name) { this->name = name;};
};

#endif