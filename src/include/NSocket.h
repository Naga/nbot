#ifndef NSOCK_H
#define NSOCK_H

#include "defines.h"
#include <sys/stat.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <errno.h>
#include <iostream>

#if defined(LINUX)
	#include <sys/socket.h>
	#include <netinet/in.h>
	#include <sys/types.h>
	#include <arpa/inet.h>
	#include <netdb.h>
	#include <sys/time.h>
	#include <unistd.h>
	#include <fcntl.h>
	#include <sys/ioctl.h>
	#define SOCKET_ERROR -1
#elif defined(WINDOWS)
	#include <io.h>
	#include <Ws2tcpip.h>
	#include "ported_functions.h"
#endif

#ifdef HAS_OPENSSL
	#include <openssl/bio.h> // BIO objects for I/O
	#include <openssl/ssl.h> // SSL and SSL_CTX for SSL connections
	#include <openssl/err.h> // Error reporting
#endif

class NSocket {
public:
	NSocket(std::string hostname, unsigned short int port, bool isSSL = false, bool ipv6_enabled=true);	
	int Connect();
	bool isSSL();
//ssl stuff
#ifdef HAS_OPENSSL
	SSL_CTX *ctx;
	SSL *ssl;
	BIO *sbio;
	std::string get_used_chiper_name();
	std::string get_used_chiper_version();
#endif

	int recv (SOCKET s, char *buf, int len, int flags);
	int Recv (std::string &str, int len);
	int RecvUntil(std::string& data, std::string terminator = "\n");

	int send (SOCKET s, const char *buf, int len, int flags);
	int Send(std::string data);
	int SendLine(std::string data);

	int available();
	bool Connected() { return is_connected; }
	bool start();

	void Disconnect();
	void Close();
	bool setBlockState(bool blocking);
	void waitfordata(unsigned int timeout);
private:
//ssl stuff
#ifdef HAS_OPENSSL
	SSL_CTX * initSSLNsocket();
	bool ConnectSSL();
	void freeSSL();
#endif

	SOCKET _s;
	bool b_isSSL;
	bool b_ipv6_enabled;
	std::string hostname;
	unsigned short int port;
	bool is_connected;
	struct sockaddr_in serv_addr;
#ifdef WIN32
	WSADATA	hWSAData;
#endif 
};

#endif
