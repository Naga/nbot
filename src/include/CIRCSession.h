#include "NString.h"
#include "NSocket.h"
#include "CSemaphore.h"
#include "EventMgr.h"
#include <thread>
#include "CQueue.hpp"
class IRCUser;
class IRCChannel;
class IRCMessage;

#ifndef _CIRCSESSION_H
#define _CIRCSESSION_H

void SendThread(void *param);
void RecvThread(void *param);
void ProcessThread(void *param);

class IRCSession {
	public :
		bool ProcessIRCString(NString line);
		bool Connect(bool reconnect=false);
		void Disconnect(NString qMsg="Eh, wtf?!", bool forced = false, bool reconnect = false);
		bool JoinChannel(NString sChannelName);
		bool LeaveChannel (NString channel, NString message="");
		bool ChangeNick (NString newNick);
		void handleCTCP(IRCMessage* msg);
		bool getConnectionState();
		bool isDisconnecting() { return disconnecting; }

		NString getServerName();

		NSocket* getSocket();

		IRCSession(NString server, NString password, int port, NString botnick, NString realname, bool useSSL);
		IRCSession(CServerConfig * conf);

		std::vector<IRCChannel*> chans;
		IRCChannel* FindChannel(NString name);
		IRCChannel* GetOrCreateChan(NString name);

		std::vector<IRCUser*> users;
		IRCUser* FindUser(NString name);
		IRCUser* FindUserByMask(NString mask);
		IRCUser* GetOrCreateUser(NString name);

		void ProcessRecvQueue();
		void ProcessSendQueue();

		void PutQuick(NString line, ...);
		void PutServ(NString line, ...);

		NString getBotNick() { return botnick; }
		void SetBotNick(NString name) {botnick = name;}

		void registerChatCmd(NString trigger, NString cmd, callback func);
		std::map<NString, callback> cmds;

		CSemaphore *send_semaphore;
		CSemaphore *process_semaphore;

		CServerConfig * getConfig() { return config; }

		CQueue<NString> in_queue;
		CQueue<NString> out_queue;

	private:
		bool connected;
		unsigned short int nick_trys;
		NString prefixes;
		std::map<NString, NString> serverModeMap;
		NSocket *m_socket;
		std::thread *recv_thread;
		std::thread *send_thread;
		std::thread *process_thread;
		NString server;
		NString botnick;
		NString realname;
		NString password;
		int port;
		bool useSSL;
		bool ipv6_enabled;
		CServerConfig * config;
		bool disconnecting; //disconnect is in progress, do not reconect on ERROR (which follows QUIT)
		uint8_t max_reconnect_trys;
		uint8_t reconnect_try;
};

#endif
