#ifndef _EVENTMGR_H
#define _EVENTMGR_H
#include <deque>
class Event;
class EventMgr;
class IRCSession;
class IRCUser;
class IRCChannel;
class IRCMessage;
struct SQLCommand;
typedef void (*callback)(Event* evt);


#include "main.h"
#include "ChatCommands.h"

class CPlugin;

typedef void (*plugincallback)(CPlugin* plugin, Event* evt);
struct plugin_callback_set {
	CPlugin* plugin;
	plugincallback func;
	plugin_callback_set(CPlugin* _plugin, plugincallback _func) :
		plugin(_plugin), func(_func) {};
};

struct choice; //forward

typedef enum {
	EVENT_GENERIC,
	EVENT_CONNECTED = 1,
	EVENT_CONNECTED2, //end of modt, ready to send joins/...
	EVENT_NICKCHANGE,
	EVENT_NICKSERV,
	EVENT_PRIVMSG,

	EVENT_ONJOIN,
	EVENT_ONNICKCHANGE,
	EVENT_ONKICK,
	EVENT_ONPART,
	EVENT_ONQUIT,
	EBENT_ONMODECHANGE,
	EVENT_ONBAN,
	EVENT_ONUNBAN,
	EVENT_ONINVITE,
	EVENT_ONERROR,
	EVENT_ONKILL,
	EVENT_ONPING,
	EVENT_ONQUERY,

	EVENT_CTCP, //fired if message is \001message\001
	EVENT_CTCP_ACTION, //invoked by EVENT_CTCP
	EVENT_ALL, //call on every event, used for plugin manager
} EventType;


class EventMgr {
private:
	IRCSession* network;
	std::deque<int> eventqueue;

	std::map<int, std::vector<callback> > callbacks;
	std::map<int, std::vector<plugin_callback_set*> > plugincallbacks;
	std::map<EventType, std::vector<std::pair<NString, callback>>> rxcallbacks;
	static EventMgr* instance;
private:
    static bool instance_exists;
    EventMgr() {
        network = NULL;
    }
public:
	void fire(EventType event_type, IRCMessage* msg = NULL,IRCChannel* chan = NULL, IRCUser* user = NULL);
	bool RegisterCallback(int type, callback func);
	bool RegisterPluginCallback(int type, CPlugin* plugin, plugincallback func);
#if defined(HAS_STD_REGEX) || defined(HAS_BOOST)
	bool RegisterRegexMsgCallback(EventType type, NString regex, callback func);
#endif
	void clearAll() {eventqueue.clear(); callbacks.clear(); rxcallbacks.clear();}
	void SetSession (IRCSession* net);
    static EventMgr* getInstance();
    ~EventMgr() {
        instance_exists = false;
    }
};



class Event {
public:
	struct info {
		NString trigger;
		NString cmdname;
	} cmdinfo;
	EventType event_type;
	IRCSession* session;
	IRCMessage* msg;
	IRCChannel* chan;
	IRCUser* user;
	Event(IRCSession* session) {this->session = session;}
	Event(IRCSession* session, IRCMessage* msg) {this->session = session; this->msg = msg;}
	Event(IRCSession* session, IRCMessage* msg, IRCChannel* chan) {this->session = session; this->msg = msg; this->chan = chan;}
	Event(IRCSession* session, IRCMessage* msg, IRCChannel* chan, IRCUser* user) {this->session = session; this->msg = msg; this->chan = chan; this->user = user;}
};

#endif