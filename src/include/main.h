#ifndef MAIN_H
#define MAIN_H

#include "defines.h"

#ifdef WINDOWS
		#define _WINSOCKAPI_    // stops windows.h including winsock.h
        #include <Windows.h>
		#include <WinSock2.h>
        BOOL CtrlHandler( DWORD fdwCtrlType );
#else
        #include <sys/types.h>       // For data types
        #include <signal.h>          // to intercept signals (close, ctrl-c etc)
        void sighandler (int signal);
        #include <pthread.h>
        #include <cerrno>
        #include <sys/ioctl.h>
#endif


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>
#include <ios>
#include <cstdarg>
#include <iostream>
#include <vector>
#include <time.h>
#include <algorithm>
#include <sstream>

#include "NString.h"
#include "CConfig.h"
#include "EventMgr.h"
#include "events.h"
#include "CPluginManager.h"
#ifdef HAS_MYSQL
#	include "mysql.h"
#endif

void Shutdown();
void SendDebugOutput(NString out, ...);
void SetDbgConsoleTitle(NString title, ...);
NString get_last_error(int error = -1);
#endif
