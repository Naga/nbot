#ifndef _NBOT_CPLUGINMGR_H
#define _NBOT_CPLUGINMGR_H

#include "CIRCChannel.h"
#include "CIRCCommand.h"
#include "ChatCommands.h"
#include "CIRCUser.h"
#include "CSemaphore.h"
#include "CPlugin.h"
#include "EventMgr.h"

#ifdef WINDOWS
#	define MODHANDLE HINSTANCE
#	define MOD_INIT_FUNC_HANDLE FARPROC
#	define FREE_PLUGIN(x) FreeLibrary(x)
#else
#	define MODHANDLE void*
#	define MOD_INIT_FUNC_HANDLE  void *
#	define FREE_PLUGIN(x) dlclose(x)
#	include <dlfcn.h>
#endif


NString get_libload_error();

class CPlugin;

struct plugin_info {
	MODHANDLE modh;
	std::string name;
	CPlugin* plugin;

	plugin_info(MODHANDLE _modh, std::string _name, CPlugin* _plugin) : 
		modh(_modh), name(_name), plugin(_plugin) {};
};


class CPluginManager {
private:
	NString last_error;
	static CPluginManager* instance;
	std::map<IRCSession *, std::vector<plugin_info*>*> plugins; 
	static bool instance_exists;
public:
	std::vector<plugin_info*>* getLoadedPlugins(IRCSession* session) {return plugins[session];}
	NString getLastError() {return last_error;}
	static CPluginManager* getInstance();
	bool Unload(IRCSession* session, NString name);
	bool Load(IRCSession* session, NString path);
	plugin_info *findPlugin(IRCSession* session, NString name);
	void notify(Event* evt);
};

#endif