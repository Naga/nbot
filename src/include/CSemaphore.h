#include <mutex>
#include <condition_variable>

#ifndef _NBOT_SEMAPHORE_H
#define _NBOT_SEMAPHORE_H

// binary semaphore
class CSemaphore {
public:
    explicit CSemaphore(int init_count = count_max) : count_(init_count) {}

    // P-operation
	void wait();
    bool try_wait();
    // V-operation / release
    void signal();

    // Lockable requirements
    void lock() { wait(); }
    bool try_lock() { return try_wait(); }
    void unlock() { signal(); }

private:
    static const int count_max = 1;
    int count_;
    std::mutex m_;
    std::condition_variable cv_;
};

#endif