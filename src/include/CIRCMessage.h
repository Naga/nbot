#include "NString.h"



#ifndef _CIRCMESSAGE_H
#define _CIRCMESSAGE_H

class IRCMessage {
public :
	struct info	{
		bool isServer;
		NString nick, user, host;
	} sender;

	NString code;
	NString message;
	NString raw;
	bool isCommand;
	IRCMessage() {};

	void Reset();

private :
	void ParseIRCCommand(NString line);
};

#endif