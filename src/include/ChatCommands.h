#ifndef _CHAT_COMMANDS_H
#define _CHAT_COMMANDS_H

#include "EventMgr.h"
#include "events.h"

void HandleHopCmd(Event* evt);
void HandleDlTimeCmd(Event *evt);
void HandleJoinCmd(Event *evt);
void HandleNickCmd(Event *evt);
void HandlePartCmd(Event *evt);
void HandleDebugCmd(Event *evt);
void HandleLoadCmd(Event *evt);
void HandleUnloadCmd(Event *evt);
void HandleReloadCmd(Event *evt);
#endif