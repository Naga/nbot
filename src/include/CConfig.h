#ifndef _CONFIG_H
#define _CONFIG_H

#include "NString.h"

#include <fstream>
#include <cstdlib>
#include <stdint.h>

class CConfig {
protected:
	std::map<NString, NString> confmap;
	CConfig() {};
public:
	void add(NString key, NString val);
	bool key_exists(NString key);	
	NString GetString(NString key) {return (key_exists(key) ? confmap[key] : "");}
	const char * GetCString(NString key) { return (key_exists(key) ? confmap[key].c_str() : "");}
	bool GetBoolean(NString key) { return (key_exists(key) && (confmap[key].asLower() == "true" || confmap[key].asLower() == "yes" || confmap[key] == "1")) ? true : false;}
	int GetInt(NString key) {return (key_exists(key) && (atoi(confmap[key].c_str()) != 0)) ? atoi(confmap[key].c_str()) : 0;}
	double GetFloat(NString key) {return (key_exists(key) && (strtod(confmap[key].c_str(), NULL) != 0)) ? strtod(confmap[key].c_str(), NULL) : 0.00;}
};

class CPluginConfig : public CConfig {
	public:
		CPluginConfig(std::map<NString, NString> config) {confmap = config;}
		NString getServerName() {return GetString("plugin_name");}
};

class CServerConfig : public CConfig {
	private:
		std::map<NString, CPluginConfig*> plugin_confmap;
	public:
		std::map<NString, CPluginConfig*> getPluginMap() { return plugin_confmap; }
		CServerConfig(std::map<NString, NString> config) {confmap = config;}
		NString getServerName() {return GetString("server_name");}
		void addPluginConfig(NString pluginname, CPluginConfig *plugin);
		void delPluginConfig(NString pluginname) {plugin_confmap.erase(pluginname);}
		CPluginConfig * getPluginConfig(NString pluginname);
};




class CConfigMgr {
private:
	NString filename;
	static CConfigMgr* instance;
    CConfigMgr() {}
public:
	std::map<NString, CServerConfig*> confmap; //public to get in main
	void addConfig(NString server, CServerConfig* config);
	bool LoadFile(NString filename, bool reload = false);
	void Reload();

	CServerConfig * getServerConfig(NString servername);
	static CConfigMgr* getInstance();
};


#endif