#include "NString.h"
#include "CIRCSession.h"
#include "CIRCChannel.h"


#ifndef _CIRCUSER_H
#define _CIRCUSER_H


class IRCUser {
	friend class IRCChannel;
	friend class IRCSession;

	protected:
		NString nick;
		NString host;
		NString ident;
		NString server;
		NString realname;

		std::map<NString, IRCChannel*> chanlist; //<#void, IRCChanne<#void>>
		NString modes;
		
		bool identified;
		int idle;
		int signon;

	public:
		bool isOnChan(NString name);
		bool sendMessage();
		bool sendNotice();
		bool kickFromChannel(const char * channel);
		bool giveMode(const char* modes);
		bool takeMode(const char* modes);
		NString getNick() {return nick;}
		NString getHost() {return host;}
		NString getRealName() {return realname;}
		NString getServer() {return server;}
		NString getIdent() {return ident;}
		NString getModes() {return modes;}

		IRCUser(NString name) { this->nick = name;}
		IRCUser(NString name, NString host, NString ident, NString realname) {this->nick = name; this->host = host; this->ident = ident; this->realname = realname;}
};

#endif