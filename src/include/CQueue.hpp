#include <deque>
#include <mutex>

#ifndef _CQUEUE_H
#define _CQUEUE_H


template<typename T>
class CQueue{
public:
	unsigned int size() {
		std::lock_guard<std::mutex> lock(mtx);
		return queue.size();
	};

	void clear() {
		std::lock_guard<std::mutex> lock(mtx);
		queue.clear();
	}

	T front() {
		std::lock_guard<std::mutex> lock(mtx);
		return queue.front();
	}

	void push_front(T object) {
		std::lock_guard<std::mutex> lock(mtx);
		queue.push_front(object);
	}

	void push_back(T object) {
		std::lock_guard<std::mutex> lock(mtx);
		queue.push_back(object);
	}

	void pop_front() {
		std::lock_guard<std::mutex> lock(mtx);
		queue.pop_front();
	}
	void pop_back() {
		std::lock_guard<std::mutex> lock(mtx);
		queue.pop_back();
	}

	bool empty() {
		std::lock_guard<std::mutex> lock(mtx);
		return queue.empty();
	}

private:
    std::deque<T> queue;
    std::mutex mtx;
};

#endif