#ifndef NSTRING_H
#define NSTRING_H

#include <map>
#include <vector>
#include <algorithm>
#include <sstream>
#include <string>

class NString;

class NString : public std::string {
public:
	explicit NString(bool b) : std::string(b ? "true" : "false") {}
	explicit NString(char c);
	explicit NString(unsigned char c);
	explicit NString(short i);
	explicit NString(unsigned short i);
	explicit NString(int i);
	explicit NString(unsigned int i);
	explicit NString(long i);
	explicit NString(unsigned long i);
	explicit NString(long long i);
	explicit NString(unsigned long long i);
	explicit NString(double i, int precision = 2);
	explicit NString(float i, int precision = 2);

	NString() : std::string() {}
	NString(const char* c) : std::string(c) {}
	NString(const char* c, size_t l) : std::string(c, l) {}
	NString(const std::string& s) : std::string(s) {}
	NString(size_t n, char c) : std::string(n, c) {}
	~NString() {}

	NString& toUpper();
	NString& toLower();
	unsigned int toUInt() const;
	static unsigned int toUInt(NString str);

	NString asUpper() const;
	NString asLower() const;

	NString Left(size_t num) const;
	NString Right(size_t num) const;

	NString cut_right(size_t len);
	NString cut_left(size_t len);

	std::vector<NString> split_delim(NString delim = " ");
	std::vector<NString> split_delim_r(NString source, NString delim = " ");

	NString cut_to_char(NString src, char end);
	void cut_to_char(char end);
	void htmlEntitiesDecode();
	NString substr(size_type _Off = 0, size_type _Count = npos) const;


	NString Trim_r(const NString trim_charss = " \t\r\n");
	NString TrimgRight_r(const NString trim_chars = " \t\r\n");
	NString TrimLeft_r(const NString trim_chars = " \t\r\n");

	NString Trim(const NString trim_chars = " \t\r\n");
	NString TrimgRight(const NString trim_chars = " \t\r\n");
	NString TrimLeft(const NString trim_chars = " \t\r\n");

	NString Token(size_t pos);
	int find_count(const char * search);
	static bool WildCmp(const NString mask, const NString str);
	bool WildCmp(const NString mask);
};
#endif