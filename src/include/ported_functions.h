#include "defines.h"

#if (NTDDI_VERSION < NTDDI_VISTA)
	int inet_pton(int af, const char *src, void *dst);
	static int inet_pton4(const char *src, unsigned char *dst);
	static int inet_pton6(const char *src, unsigned char *dst);
#endif