#ifndef _EVENT_H
#define _EVENT_H

#include "EventMgr.h"
#include "CConfig.h"
class Event;
void JoinConfigChans(Event* e);
void HandlePrivMsg(Event *e);

void callModuleManager(Event* evt);
void HandleCTCP(Event *e);
void nickserv_identify(Event *evt);
void RegisterCommands(Event *e);
void SetDefaultModes(Event* evt);

#endif
