#ifndef _NBOT_CPLUGIN_H
#define _NBOT_CPLUGIN_H

#include "CIRCChannel.h"
#include "CIRCCommand.h"
#include "ChatCommands.h"
#include "CIRCUser.h"
#include "CSemaphore.h"
#include "CPluginManager.h"

#ifdef WINDOWS
#	define MODULE_EXPORT extern "C" __declspec(dllexport)
#else
#	define MODULE_EXPORT extern "C" 
#endif

class CPlugin {
	NString name;
	NString description;
	bool reloadable;
public:
	void setInfo(NString modname, NString desc) {name = modname; description = desc;}
	NString getName() {return name;}
	NString getDescription() {return description;}
	virtual void OnLoad(IRCSession *sess) {}
	virtual void OnConnected(IRCSession *session) {}
	virtual void OnDisconnect() {}

	virtual void OnPrivMessage(IRCChannel *chan, IRCUser *user, IRCMessage * msg) {}
	virtual void OnQuery(IRCUser *user, IRCMessage * msg) {}
	virtual void OnNotice(IRCChannel *chan, IRCUser *user,  IRCMessage* msg) {}
	virtual void OnKick(IRCChannel *chan, IRCUser *user,  IRCMessage* msg) {}
	virtual void OnJoin(IRCChannel *chan, IRCUser *user) {}
	virtual void OnPart(IRCChannel *chan, IRCUser *user,  IRCMessage* msg) {}
	virtual void OnError(IRCMessage * msg) {}
	virtual void OnPing(IRCMessage *msg) {}
	virtual void OnKill(IRCMessage *msg) {}
	virtual void OnModeChange(IRCChannel *chan, IRCUser *user, IRCMessage *msg) {}
	virtual void OnBan(IRCChannel *chan, IRCUser *user, IRCMessage* msg) {}
	virtual void OnUnBan(IRCChannel *chan, IRCUser *user,  IRCMessage* msg) {}
	virtual void OnQuit(IRCChannel *chan, IRCUser *user,  IRCMessage* msg) {}
	virtual void OnInvite(IRCChannel *chan, IRCUser *user) {}
	virtual void OnCTCP(IRCChannel *chan, IRCUser *user,  IRCMessage* msg) {}
	virtual void OnCTCPAction(IRCChannel *chan, IRCUser *user,  IRCMessage* msg) {}
	virtual void OnNickServ(IRCMessage *msg) {}
	virtual void OnNickChange(IRCUser *user,  IRCMessage* msg) {}
};

#endif