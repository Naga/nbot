#include "NString.h"


#ifndef _CIRCCOMMAND_H
#define _CIRCCOMMAND_H

class IRCCommandInfo {
private:
	const char* trigger;
	NString command;
	unsigned int callcounter;
public:
	NString GetCommandName() {return command;}
	NString GetTrigger() {return trigger;}
	void SetTrigger(const char * triggerchar) { trigger = triggerchar; }
	void SetCommandName(NString name) { command = name;}
	int GetTimesCalled() {return callcounter;}
	void IncrCounter() {++callcounter;}
	IRCCommandInfo(const char * trigger, NString command) {
        this->trigger = trigger;
        this->command = command;
	};
};

#endif
