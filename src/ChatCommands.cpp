#include "include/ChatCommands.h"
#include "include/EventMgr.h"
#include "include/CIRCSession.h"
#include "include/CIRCUser.h"
#include "include/CIRCChannel.h"
#include "include/CIRCMessage.h"
#include "include/defines.h"

void RegisterCommands(Event* evt) {
	IRCSession* irc = evt->session;
	irc->registerChatCmd("!", "join", HandleJoinCmd);
	irc->registerChatCmd("!", "part", HandlePartCmd);
	irc->registerChatCmd("!", "hop", HandleHopCmd);
	irc->registerChatCmd("!", "nick", HandleNickCmd);
	irc->registerChatCmd("!", "debug", HandleDebugCmd);
	irc->registerChatCmd("!", "load", HandleLoadCmd);
	irc->registerChatCmd("!", "unload", HandleUnloadCmd);
	irc->registerChatCmd("!", "reload", HandleReloadCmd);
	irc->registerChatCmd("!", "dltime", HandleDlTimeCmd);
}

void HandleHopCmd(Event* evt) {
	if (evt->user->getNick() != evt->session->getConfig()->GetString("bot_owner").c_str()) {
		evt->session->PutServ("NOTICE %s :not allowed", evt->user->getNick().c_str());
		return;
	}

	int pos1 = evt->msg->message.find_first_of("#");
	int pos2 = evt->msg->message.find_first_of(" ");

	if (pos1 == std::string::npos) {
		evt->session->PutServ("NOTICE %s : Syntax: !hop #channel", evt->user->getNick().c_str());
		return;
	}

	NString chan = evt->msg->message.substr(pos1, (pos2 != std::string::npos) ? pos2-pos1 : pos2);
	evt->session->LeaveChannel(chan);
	evt->session->JoinChannel(chan);
}

void HandleLoadCmd(Event *e) {
	if (e->user->getNick() != e->session->getConfig()->GetString("bot_owner").c_str()) {
		e->session->PutServ("NOTICE %s :not allowed", e->user->getNick().c_str());
		return;
	}

	NString type = e->msg->message.Token(0).asLower();

	if (type == "plugin") {
		CPluginManager *pluginmgr = CPluginManager::getInstance();
		NString name = e->msg->message.Token(1);

		if (pluginmgr->findPlugin(e->session, name)) {
			e->session->PutServ("PRIVMSG %s :Plugin already loaded in this session!", e->chan->getName().c_str());
			return;
		}

		name = MODULE_PREFIX+name+MODULE_EXT;

		if (pluginmgr->Load(e->session, name)) {
			e->session->PutServ("PRIVMSG %s :Plugin loaded!", e->chan->getName().c_str());
		} else {
			e->session->PutServ("PRIVMSG %s :Error loading plugin: %s", e->chan->getName().c_str(), pluginmgr->getLastError().c_str());
		}
	}
}

void HandleUnloadCmd(Event *e) {
	if (e->user->getNick() != e->session->getConfig()->GetString("bot_owner").c_str()) {
		e->session->PutServ("NOTICE %s :not allowed", e->user->getNick().c_str());
		return;
	}

	NString type = e->msg->message.Token(0).asLower();

	if (type == "plugin") {
		CPluginManager *pluginmgr = CPluginManager::getInstance();
		NString name = e->msg->message.Token(1);

		if (!pluginmgr->findPlugin(e->session, name)) {
			e->session->PutServ("PRIVMSG %s :Plugin not loaded in this session!", e->chan->getName().c_str());
			return;
		}

		if (pluginmgr->Unload(e->session, name)) {
			e->session->PutServ("PRIVMSG %s :Plugin unloaded!", e->chan->getName().c_str());
		} else {
			e->session->PutServ("PRIVMSG %s :Error unloading plugin", e->chan->getName().c_str());
		}
	}
}

void HandleReloadCmd(Event *e) {
	if (e->user->getNick() != e->session->getConfig()->GetString("bot_owner").c_str()) {
		e->session->PutServ("NOTICE %s :not allowed", e->user->getNick().c_str());
		return;
	}
	NString type = e->msg->message.Token(0).asLower();
	if (type == "plugin") {
		CPluginManager *pluginmgr = CPluginManager::getInstance();
		NString name = e->msg->message.Token(1);

		if (!pluginmgr->findPlugin(e->session, name)) {
			e->session->PutServ("PRIVMSG %s :Plugin not loaded in this session!", e->chan->getName().c_str());
			return;
		}
		if (!pluginmgr->Unload(e->session, name)) {
			e->session->PutServ("PRIVMSG %s :Error unloading plugin", e->chan->getName().c_str());
			return;
		}
		NString pluginname = MODULE_PREFIX+name+MODULE_EXT;
		if (!pluginmgr->Load(e->session, pluginname)) {
			e->session->PutServ("PRIVMSG %s :Error loading plugin: %s", e->chan->getName().c_str(), pluginmgr->getLastError().c_str());
			return;
		}
		e->session->PutServ("PRIVMSG %s :Plugin '%s' reloaded!", e->chan->getName().c_str(), name.c_str());
	}
}

void HandleDlTimeCmd(Event *evt) {

	NString msg = evt->msg->message;

	if (msg.split_delim().size() < 2 || msg.Token(0).length() < 2) {
		evt->session->PutServ("NOTICE %s :Syntax: !dltime filesize download_speed", evt->chan->getName().c_str());
	}

	NString sizetype = msg.Token(0).substr(msg.Token(0).length()-2, std::string::npos);
	std::istringstream ss(msg.Token(0).substr(0, msg.length()-2));
	long size;

	ss >> size;

	if (size == 0) {
		evt->session->PutServ("NOTICE %s :Error! Div/0", evt->user->getNick().c_str());
		return;
	}

	long size_in_kb = size; //failsafe

	if (sizetype.empty())
		sizetype = "KB";

	if (sizetype == "TB" || sizetype == "TB")
		size_in_kb = size*(1024*1024*1024);
	if (sizetype == "GB" || sizetype == "gb")
		size_in_kb = size*(1024*1024);
	if (sizetype == "MB" || sizetype == "mb")
		size_in_kb = size*1024;
	if (sizetype == "KB" || sizetype == "kb")
		size_in_kb = size;
	if (sizetype == "B" || sizetype == "b")
		size_in_kb = size/1024;

	long dl_speed = atoi(msg.Token(1).c_str());

	if (size_in_kb == 0 || dl_speed == 0) {
		evt->session->PutServ("NOTICE %s :Error! Div/0", evt->user->getNick().c_str());
		return;
	}

	long time_in_sec = (size_in_kb / dl_speed);

	if (time_in_sec == 0) { //failsafe
		evt->session->PutServ("NOTICE %s :Error! Div/0", evt->user->getNick().c_str());
		return;
	}

	int days, hours, minutes, seconds;
	days = time_in_sec / 60 / 60 / 24;
	hours = (time_in_sec / 60 / 60) % 24;
	minutes = (time_in_sec / 60) % 60;
	seconds = time_in_sec % 60;

	evt->session->PutServ("PRIVMSG %s : Download time: %d Days, %d Hours, %d Minutes, %d seconds", evt->chan->getName().c_str(), days, hours, minutes, seconds);
}

void HandleDebugCmd(Event *e) {
	std::vector<NString> tokens = e->msg->message.split_delim(" ");
	if (tokens.size() < 2) {
		e->session->PutServ("NOTICE %s :Syntax: !debug type name [private/public]", e->user->getNick().c_str());
		return;
	}
	NString type = tokens[0];
	NString target;
	NString mode;

	if (tokens.size() == 2 || tokens[2] == "private") {
		target= e->user->getNick().c_str();
		mode = "NOTICE";
	} else {
		target = e->chan->getName();
		mode = "PRIVMSG";
	}

	if (type == "nick" || type == "name") {
		if (IRCUser *usr = e->session->FindUser(tokens[1])) {
			NString nick = usr->getNick();
			NString realname = usr->getRealName();
			NString ident = usr->getIdent();
			NString server = usr->getServer();
			NString host = usr->getHost();
			NString modes = usr->getModes();
			e->session->PutServ("%s %s :Username: %s, RealName: %s, Ident: %s, Server: %s, Host: %s, Modes: %s",mode.c_str(), target.c_str(),  nick.c_str(), realname.c_str(), ident.c_str(), server.c_str(), host.c_str(), modes.c_str());
			return;
		} else {
			e->session->PutServ("%s %s :User not found!", mode.c_str(), target.c_str());
			return;
		}
	} else if (type == "chan" || type == "channel") {
		if (IRCChannel *chan = e->session->FindChannel(tokens[1])) {
			NString name = chan->getName();
			NString topic = chan->getTopic();
			NString modes = chan->getModes();
			unsigned int usrcount = chan->getUserCount();
			e->session->PutServ("%s %s :Channelname: %s, Topic: %s, Modes: %s, UserCount: %d", mode.c_str(), target.c_str(), name.c_str(), topic.c_str(), modes.c_str(), usrcount);
			return;
		} else {
			e->session->PutServ("%s %s :Channel not found!", mode.c_str(), target.c_str());
			return;
		}
	} else if (type == "finduserbymask" || type == "userbymask") {
	//FindUserByMask
		NString mask = tokens[1];
		if (IRCUser *usr = e->session->FindUserByMask(mask)) {
			NString nick = usr->getNick();
			NString realname = usr->getRealName();
			NString ident = usr->getIdent();
			NString server = usr->getServer();
			NString host = usr->getHost();
			NString modes = usr->getModes();
			e->session->PutServ("%s %s :Username: %s, RealName: %s, Ident: %s, Server: %s, Host: %s, Modes: %s",mode.c_str(), target.c_str(),  nick.c_str(), realname.c_str(), ident.c_str(), server.c_str(), host.c_str(), modes.c_str());
			return;
		} else {
			e->session->PutServ("%s %s :User not found!", mode.c_str(), target.c_str());
			return;
		}
	} else if (type == "plugins" || type == "plugin") {
		NString cmd = tokens[1];
		
		if (cmd == "loaded") {
			CPluginManager *pluginmgr = CPluginManager::getInstance();
			std::vector<plugin_info*>* plugins = pluginmgr->getLoadedPlugins(e->session);
			NString plugin_list;
			for (std::vector<plugin_info*>::iterator it = plugins->begin(); it != plugins->end(); ++it) {
				plugin_list += (*it)->name+", ";
			}
			plugin_list.erase(plugin_list.length()-2, std::string::npos);
			e->session->PutServ("%s %s : Loaded plugins: %s", mode.c_str(), target.c_str(), plugin_list.c_str());
		}
		
	}
}

void HandleJoinCmd(Event *evt) {
	if (evt->user->getNick() != evt->session->getConfig()->GetString("bot_owner").c_str()) {
		evt->session->PutServ("NOTICE %s :not allowed", evt->user->getNick().c_str());
		return;
	}

	NString chan = evt->msg->message.Trim_r();
	if (chan[0] == '#')
		if (chan.find_first_of(" ") != std::string::npos)
			evt->session->PutServ("JOIN %s", chan.substr(0, chan.find_first_of(" ")).c_str());
		else
			evt->session->PutServ("JOIN %s", chan.c_str());
}
void HandlePartCmd(Event *evt) {
	if (evt->user->getNick() != evt->session->getConfig()->GetString("bot_owner").c_str()) {
		evt->session->PutServ("NOTICE %s :not allowed", evt->user->getNick().c_str());
		return;
	}

	NString chan = evt->msg->message.Trim_r();
	if (chan[0] == '#')
		if (chan.find_first_of(" ") != std::string::npos)
			evt->session->LeaveChannel(chan.substr(0, chan.find_first_of(" ")).c_str(), chan.substr(chan.find_first_of(" ")));
		else
			evt->session->LeaveChannel(chan.c_str());
}

void HandleNickCmd(Event *evt) {
	IRCSession *irc = evt->session;
	NString nick = evt->msg->message.Trim_r();

	if (evt->user->getNick() != evt->session->getConfig()->GetString("bot_owner").c_str()) {
		evt->session->PutServ("NOTICE %s :not allowed", evt->user->getNick().c_str());
		return;
	}

	if (!irc->FindUser(nick))
		irc->ChangeNick(nick);
	else
		irc->PutServ("NOTICE %s :Can't change Nickname - User %s already taken.", evt->user->getNick().c_str(), nick.c_str());
}
