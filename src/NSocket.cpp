#include "include/NSocket.h"

#if defined(HAS_OPENSSL) && defined(WINDOWS)
	#include <openssl/applink.c> // To prevent crashing (see the OpenSSL FAQ)
#endif

NSocket::NSocket(std::string hostname, unsigned short int port, bool b_isSSL/* = false*/, bool ipv6_enabled/*=true*/) {
	this->hostname = hostname;
	this->port = port;
	this->b_isSSL = b_isSSL;
	this->is_connected = false;
	this->b_ipv6_enabled = ipv6_enabled;
}

bool NSocket::start() {
#ifdef HAS_OPENSSL
	if (!b_isSSL) {
#endif
		if (Connect() == SOCKET_ERROR)
			return false;
		else
			return true;
#ifdef HAS_OPENSSL
	} else
		return ConnectSSL();
#endif
}

int NSocket::Connect() {
	char addrstr[200];
	int s;
	struct addrinfo hints;
	struct addrinfo *res=0, *rp;
	void *ptr;
	unsigned char type;
	
	memset (&hints, 0, sizeof (hints));
	hints.ai_family = b_ipv6_enabled ? PF_UNSPEC : PF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags |= AI_CANONNAME;

	char c_port[6];
	snprintf( c_port, 6, "%d", port);
  
	s = getaddrinfo(hostname.c_str(), c_port, &hints, &res);
	if (s != 0)
		return SOCKET_ERROR;

	for (rp = res; rp != NULL; rp = rp->ai_next) {
		
		
		switch (rp->ai_family) {
			case AF_INET:
				ptr = &((struct sockaddr_in *) rp->ai_addr)->sin_addr;
				break;
			case AF_INET6:
				ptr = &((struct sockaddr_in6 *) rp->ai_addr)->sin6_addr;
				break;
		}
		inet_ntop (rp->ai_family, ptr, addrstr, sizeof(addrstr));
		std::cout << "trying ip: " << addrstr << std::endl;
		
        _s = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
        if (_s == -1)
            continue;

		setBlockState(false);
		int con_ret = ::connect(_s, rp->ai_addr, rp->ai_addrlen); 

		if (con_ret == 0) {
			timeval timeout;
			fd_set writeFds;
			FD_ZERO(&writeFds);
			FD_SET(_s, &writeFds);

			#ifndef CONNECTION_TIMEOUT
				#define CONNECTION_TIMEOUT 30
			#endif

			timeout.tv_sec  = CONNECTION_TIMEOUT;
			timeout.tv_usec = 0;

			int ret = select(_s+1, NULL, &writeFds, NULL, &timeout);

			if (ret > 0 && FD_ISSET(_s, &writeFds)) {
				is_connected = true;
				setBlockState(true);
				return true;
			}
		}

		close(_s);
	}

	std::cerr << "Connecting failed: " << errno << " (" << strerror(errno) << ")" <<std::endl;
	return SOCKET_ERROR;
}

bool NSocket::setBlockState(bool blocking) {
   if (!Connected()) return false;

#ifdef WINDOWS
	unsigned long mode = blocking ? 0 : 1;
	return (ioctlsocket(_s, FIONBIO, &mode) == 0) ? true : false;
#else
	int flags = fcntl(_s, F_GETFL, 0);
	if (flags < 0) return false;
	flags = blocking ? (flags&~O_NONBLOCK) : (flags|O_NONBLOCK);
	return (fcntl(_s, F_SETFL, flags) == 0) ? true : false;
#endif
}

int NSocket::RecvUntil(std::string& data, std::string terminator) {
	if (!is_connected) 
		return SOCKET_ERROR;
	int len = 0;
	while (1) {
		char r;
		int retcode = recv(_s, &r, 1, 0);

		if (retcode <= 0) {
			Disconnect();
			break;
		}

		data += r;
		len++;

		if (data.find(terminator) != std::string::npos && data.find(terminator) == (data.length() - terminator.length()))
			break;
	}
	return len;
}

int NSocket::Recv (std::string &str, int len) {
	if (!is_connected) 
		return SOCKET_ERROR;

	int glen = 0;
	while (glen <= len) {
		char r;
		int retcode = recv(_s, &r, 1, 0);

		if (retcode <= 0)
			break;

		str += r;
		len++;
	}
	return glen;
}

int NSocket::recv (SOCKET s, char *buf, int len, int flags) {
#ifdef HAS_OPENSSL
	if (b_isSSL) {
		return SSL_read(ssl, buf, len);
	}
#endif
	return ::recv(s, buf, len, flags);
}

int NSocket::send (SOCKET s, const char *buf, int len, int flags) {
#ifdef HAS_OPENSSL
	if (b_isSSL) {
		return SSL_write(ssl, buf, len);
	}
#endif
	return ::send(s, buf, len, flags);
}

int NSocket::available() {
		unsigned long data_avail = 0;

        #ifdef WINDOWS
                if (ioctlsocket(_s, FIONREAD, &data_avail) != 0)
        #else 
                if (ioctl(_s, FIONREAD, &data_avail) != 0)
        #endif
					return SOCKET_ERROR;
	return data_avail;
}

int NSocket::SendLine(std::string data) {
	if ((data.length() >= strlen(LINE_ENDING)) && (data.substr(data.length()-strlen(LINE_ENDING)) != LINE_ENDING))
		data += LINE_ENDING;

	return Send(data);
}

int NSocket::Send(std::string data) {
	if (!is_connected)
		return SOCKET_ERROR;
	int sent = send(_s, data.c_str(), data.length(), 0);
	return sent;
}

void NSocket::Close() {
#ifdef WINDOWS
	closesocket(_s);
#else
	close(_s);
#endif
}

void NSocket::Disconnect() {
	Close();
#ifdef HAS_OPENSSL
	if (b_isSSL) 
		freeSSL();
#endif
	is_connected = false;
}

void NSocket::waitfordata(unsigned int timeout) {
	fd_set readfds;
	FD_ZERO(&readfds);
	FD_SET(_s, &readfds);
	timeval timeouts;
	timeouts.tv_sec = timeout;
	timeouts.tv_usec = 0;

	select(_s+1, &readfds, NULL, NULL, &timeouts);
}


//-------------- SSL STUFF
#ifdef HAS_OPENSSL

void NSocket::freeSSL() {
	SSL_shutdown(ssl);

	//dont know why this crashes, temp disable
	SSL_free(ssl);
	EVP_cleanup();
	SSL_CTX_free(ctx);
}

SSL_CTX*  NSocket::initSSLNsocket() {
	SSL_CTX *ctx;
	SSL_library_init();
	SSL_load_error_strings();
	ERR_load_BIO_strings();
	
	ctx = SSL_CTX_new(TLSv1_client_method());
    if ( ctx == NULL ) {
        ERR_print_errors_fp(stderr);
        abort();
    }

	SSL_CTX_set_verify(ctx, SSL_VERIFY_NONE, NULL);

	return ctx;
}
std::string NSocket::get_used_chiper_name() {
	return SSL_get_cipher_name(ssl);
}
std::string NSocket::get_used_chiper_version() {
	return SSL_CIPHER_get_version(SSL_get_current_cipher(ssl));
}

bool NSocket::isSSL() {
	return b_isSSL;
}
bool NSocket::ConnectSSL() {
	if (Connect() == SOCKET_ERROR)
		return false;

	ctx = initSSLNsocket();
	ssl = SSL_new(ctx);
	SSL_set_tlsext_host_name(ssl, hostname.c_str());
	SSL_set_fd(ssl, _s);
    if ( SSL_connect(ssl) == -1 /* FAIL */ ) {			/* perform the connection */
        ERR_print_errors_fp(stderr);
		return false;
	} else {
		is_connected = true;
		return true;
	}

}
#endif
