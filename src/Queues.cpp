#include "include/CConfig.h"
#include "include/CIRCSession.h"

#include <ctime>


void IRCSession::ProcessRecvQueue() {
	NString msg = in_queue.front();

	CServerConfig * config = getConfig();

	std::time_t t = std::time(NULL);
	std::tm tm = *std::localtime(&t);
	char mbstr[100];


	if (config->GetBoolean("raw_output")) {
		if(std::strftime(mbstr, 100, "[%H:%M:%S] ", std::localtime(&t)))
			std::cout << mbstr;
		std::cout << "[INC] " << msg.c_str() << std::endl;
	}

	ProcessIRCString(msg);

	if (config->GetBoolean("debug_title"))
		SetDbgConsoleTitle("[%s@%s] - InQueue size: %d, OutQueue size: %d",getBotNick().c_str(), getServerName().c_str(), in_queue.size()-1, out_queue.size());

	in_queue.pop_front();
}

void IRCSession::ProcessSendQueue() {
	NString msg = out_queue.front();

	std::time_t t = std::time(NULL);
	std::tm tm = *std::localtime(&t);
	char mbstr[100];
	if(std::strftime(mbstr, 100, "[%H:%M:%S] ", std::localtime(&t)))
		std::cout << mbstr;

	std::cout << "[OUT] " << msg.c_str() << std::endl;
	getSocket()->SendLine(msg);

	if (getConfig()->GetBoolean("debug_title"))
		SetDbgConsoleTitle("[%s@%s] - InQueue size: %d, OutQueue size: %d",getBotNick().c_str(), getServerName().c_str(), in_queue.size(), out_queue.size()-1);

	out_queue.pop_front();
}